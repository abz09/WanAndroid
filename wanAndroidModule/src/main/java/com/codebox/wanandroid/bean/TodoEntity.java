package com.codebox.wanandroid.bean;

import com.chad.library.adapter.base.entity.SectionEntity;

/**
 * @author Able
 * @date 2019/3/29
 */
public class TodoEntity extends SectionEntity<TodoBean.DatasBean> {
    public TodoEntity(boolean isHeader, String header) {
        super(isHeader, header);
    }

    public TodoEntity(TodoBean.DatasBean datasBean) {
        super(datasBean);
    }

}

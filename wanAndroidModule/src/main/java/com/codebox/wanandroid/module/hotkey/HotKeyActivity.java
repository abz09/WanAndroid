package com.codebox.wanandroid.module.hotkey;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.base.Resource;
import com.codebox.common.common.RouterPath;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.HotKeyBean;
import com.codebox.wanandroid.databinding.ActivityHotKeyBinding;
import com.codebox.wanandroid.module.hotkey.repository.HotKeyRepository;
import com.codebox.wanandroid.module.hotkey.repository.local.db.DBHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * @author admin
 */
@Route(path = RouterPath.WanAndroidRouterPath.PATH_HOTKEY)
public class HotKeyActivity extends BaseActivity<ActivityHotKeyBinding> {
    private HotKeyViewModel mHotKeyViewModel;
    private HotKeyTagAdapter mAdapter;
    private ArrayList<HotKeyBean> listData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_key);
        DBHelper.getInstance().init(this);
        HotKeyRepository.getInstance().init(this);

        initView();
        initData();
    }

    private void initView() {
        setNavigationIcon(R.drawable.ic_back);
        setTitle("热门");

        mHotKeyViewModel = ViewModelProviders.of(HotKeyActivity.this).get(HotKeyViewModel.class);
    }

    private void initData() {

        listData = new ArrayList<>();
        getHotKey();

        mAdapter = new HotKeyTagAdapter(this, listData);
        mBinding.flowLayout.setAdapter(mAdapter);
    }

    private void getHotKey() {
        mHotKeyViewModel
                .getHotKey()
                .observe(HotKeyActivity.this, this::dealHotKey);
    }

    private void dealHotKey(Resource<List<HotKeyBean>> listResource) {
        switch (listResource.status) {
            case SUCCESS:
                showContent();
                listData.addAll(listResource.data);
                mAdapter.notifyDataChanged();
                break;

            case LOADING:
                showEmpty();
                break;

            case ERROR:
                showError();
                break;

            case FAILURE:
                showError();
                break;

            default:
                break;
        }

    }
}

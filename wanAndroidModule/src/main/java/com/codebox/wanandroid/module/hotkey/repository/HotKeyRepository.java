package com.codebox.wanandroid.module.hotkey.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import com.codebox.wanandroid.bean.HotKeyBean;
import com.codebox.wanandroid.module.hotkey.repository.local.LocalHotKeyDataSource;
import com.codebox.wanandroid.module.hotkey.repository.remote.RemoteHotKeyDataSource;
import com.codebox.common.base.Resource;
import com.codebox.common.util.NetworkUtils;

import java.util.List;

public class HotKeyRepository {
    private static final HotKeyRepository instance = new HotKeyRepository();

    private HotKeyRepository() {
    }

    public static HotKeyRepository getInstance() {
        return instance;
    }


    private Context context;
    private HotKeyDataSource remoteHotKeyDataSource = RemoteHotKeyDataSource.getInstance();
    private HotKeyDataSource localHotKeyDataSource = LocalHotKeyDataSource.getInstance();

    public void init(Context context) {
        this.context = context.getApplicationContext();
    }

    public LiveData<Resource<List<HotKeyBean>>> getHotKey() {


        if (NetworkUtils.isConnected()) {
            return remoteHotKeyDataSource.queryAll();
        } else {
//            return remoteHotKeyDataSource.queryAll();
            return localHotKeyDataSource.queryAll();
        }
    }

}

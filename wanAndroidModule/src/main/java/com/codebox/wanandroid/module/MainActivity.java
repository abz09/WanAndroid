package com.codebox.wanandroid.module;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.base.EventMsg;
import com.codebox.common.common.RouterPath;
import com.codebox.common.util.AppUtils;
import com.codebox.common.util.DownloadManagerUtils;
import com.codebox.common.util.LogUtils;
import com.codebox.common.util.NotificationUtils;
import com.codebox.common.util.RxBus;
import com.codebox.common.util.ToastUtils;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.UserBean;
import com.codebox.wanandroid.databinding.ActivityMainBinding;
import com.codebox.wanandroid.databinding.ViewNavHeaderBinding;
import com.codebox.wanandroid.module.about.AboutActivity;
import com.codebox.wanandroid.module.home.HomeFragment;
import com.codebox.wanandroid.module.hotkey.HotKeyActivity;
import com.codebox.wanandroid.module.test.TestActivity;
import com.codebox.wanandroid.module.tree.TreeFragment;
import com.codebox.wanandroid.util.SpHelper;

import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import pub.devrel.easypermissions.EasyPermissions;


/**
 * @author Able
 * @date 2018/3/21
 * @description 应用主界面
 */
@Route(path = RouterPath.WanAndroidRouterPath.PATH_MAIN_ACTIVITY)
public class MainActivity extends BaseActivity<ActivityMainBinding> implements View.OnClickListener,
        EasyPermissions.PermissionCallbacks {

    private HomeFragment mHomeFragment;
    private TreeFragment mTreeFragment;

    private DownloadManagerUtils mDownloadManagerUtil;

    private static final String APK_URL = "https://express.360ssb.com/express/api/annex/download?id=216a7f7b391e444b9e113913ab7ef7f9";
    private long mDownloadId = 0;
    private ViewNavHeaderBinding headerBind;
    private boolean isLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        reqRuntimePermission();

        registerRxBus();

        initView();
    }

    private void registerRxBus() {
        Disposable disposable = RxBus.getInstance().doSubscribe(EventMsg.class, new Consumer<EventMsg>() {
            @Override
            public void accept(EventMsg eventMsg) {
                if (eventMsg.getCode() == 100) {
                    UserBean userBean = (UserBean) eventMsg.getData();
                    headerBind.tvNavUsername.setText("你好：" + userBean.getUsername());
                    LogUtils.e(eventMsg.getCode() + " : " + eventMsg.getMsg());
                    isLogin = true;
                    headerBind.btnLogout.setVisibility(View.VISIBLE);
                }
            }
        });
        RxBus.getInstance().addSubscription(this, disposable);
    }

    private void initView() {

        setTitle("玩Android");
        setNavigationIcon(R.drawable.ic_menu);

        showFragment(1);
        mDownloadManagerUtil = new DownloadManagerUtils(this);

        initListener();

        initDrawerToggle();

        initNavHeader();
    }

    /**
     * 初始化NavHeader
     */
    private void initNavHeader() {
        View headerView = mBinding.navigationView.getHeaderView(0);
        headerBind = DataBindingUtil.bind(headerView);

        if (isLogin) {
            headerBind.btnLogout.setVisibility(View.VISIBLE);
        } else {
//            headerBind.btnLogout.setVisibility(View.INVISIBLE);
        }

        headerBind.tvNavUsername.setOnClickListener(v -> {
            if (v.getId() == R.id.tv_nav_username) {
                ARouter.getInstance().build(RouterPath.WanAndroidRouterPath.PATH_LOGIN).navigation();
            }
        });
        headerBind.btnLogout.setOnClickListener(v -> {
            SharedPreferences sp = getSharedPreferences("cookies_prefs", Context.MODE_PRIVATE);
            sp.edit().clear().apply();
            isLogin = false;

            headerBind.tvNavUsername.setText("未登录");
            isLogin = false;
            headerBind.btnLogout.setVisibility(View.INVISIBLE);
        });

        headerBind.tvNavUsername.setText("你好：" + SpHelper.getUsername());
        isLogin = true;
        headerBind.btnLogout.setVisibility(View.VISIBLE);
    }

    /**
     * 添加菜单动画
     */
    private void initDrawerToggle() {
        // 参数：开启抽屉的activity、DrawerLayout的对象、toolbar按钮打开关闭的对象、描述open drawer、描述close drawer
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout,
                mBaseBinding.toolBar, R.string.drawer_open, R.string.drawer_close);
        // 添加抽屉按钮，通过点击按钮实现打开和关闭功能; 如果不想要抽屉按钮，只允许在侧边边界拉出侧边栏，可以不写此行代码
        mDrawerToggle.syncState();
        // 设置按钮的动画效果; 如果不想要打开关闭抽屉时的箭头动画效果，可以不写此行代码
        mBinding.drawerLayout.addDrawerListener(mDrawerToggle);
    }

    private void initListener() {
        mBinding.bottomNavigation.setOnNavigationItemSelectedListener(new MyOnBottomNavigationListener());
        mBinding.navigationView.setNavigationItemSelectedListener(new MyOnDrawerNavigationListener());
        // 设置侧滑菜单图标显示本来颜色
        mBinding.navigationView.setItemIconTintList(null);
    }

    /**
     * 添加右上角菜单
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * 右上角菜单按钮事件
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menuHot) {
            Intent intent2Hot = new Intent(MainActivity.this, HotKeyActivity.class);
            startActivity(intent2Hot);
        }
        if (item.getItemId() == R.id.menuSearch) {
            ToastUtils.showToast("Search");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        mDownloadManagerUtil = new DownloadManagerUtils(this);
    }


    /**
     * 底部导航菜单事件监听
     */
    private class MyOnBottomNavigationListener implements BottomNavigationView.OnNavigationItemSelectedListener {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == R.id.navigation_home) {
                showFragment(1);
                return true;
            } else if (item.getItemId() == R.id.navigation_type) {
                showFragment(2);
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 侧滑菜单事件监听
     */
    private class MyOnDrawerNavigationListener implements NavigationView.OnNavigationItemSelectedListener {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.nav_like) {
                ToastUtils.showError("like");
                sendNotification();

            } else if (id == R.id.nav_about) {
                Intent intent2About = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intent2About);

            } else if (id == R.id.nav_update) {
                ToastUtils.showError("update");
                if (mDownloadId != 0) {
                    mDownloadManagerUtil.removeCurrentTask(mDownloadId);
                }
                mDownloadId = mDownloadManagerUtil.download(APK_URL, "ShiShiBang.apk", "时时帮更新包");

            } else if (id == R.id.nav_knowledge) {
                ARouter.getInstance().build(RouterPath.KnowledgeRouterPath.PATH_KNOWLEDGE_LIST).navigation();

            } else if (id == R.id.nav_blog) {
                ARouter.getInstance().build(RouterPath.BlogRouterPath.PATH_BLOG).navigation();

            } else if (id == R.id.nav_test) {
                Intent intent2rx = new Intent(MainActivity.this, TestActivity.class);
                startActivity(intent2rx);

            } else if (id == R.id.nav_todo) {
                ARouter.getInstance().build(RouterPath.WanAndroidRouterPath.PATH_TODOLIST).navigation();
            }
            return true;
        }
    }

    private void sendNotification() {
        NotificationUtils notificationUtils = new NotificationUtils(MainActivity.this);
        notificationUtils
                .setIconId(R.drawable.ic_action_no_like)
                .setTitle("Notification")
                .setContent("适配Android 8.0")
                .setAutoCancel(true)
                .sendNotification();
    }


    /**
     * 显示fragment
     *
     * @param id 导航按钮id
     */
    private void showFragment(int id) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        hindFragment(transaction);
        switch (id) {
            case 1:
                if (mHomeFragment != null) {
                    transaction.show(mHomeFragment);
                } else {
                    mHomeFragment = new HomeFragment();
                    transaction.add(R.id.content, mHomeFragment);
                }
                break;
            case 2:
                if (mTreeFragment != null) {
                    transaction.show(mTreeFragment);
                } else {
                    mTreeFragment = new TreeFragment();
                    transaction.add(R.id.content, mTreeFragment);
                }
                break;
            // 添加default
            default:
                break;
        }
        transaction.commit();
    }

    /**
     * 隐藏fragment
     *
     * @param transaction
     */
    private void hindFragment(FragmentTransaction transaction) {
        if (mHomeFragment != null) {
            transaction.hide(mHomeFragment);
        }

        if (mTreeFragment != null) {
            transaction.hide(mTreeFragment);
        }

    }


    /**
     * 获取动态权限
     */
    private void reqRuntimePermission() {
        String[] runtimePermission = {Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO, Manifest.permission.REQUEST_INSTALL_PACKAGES};
        if (EasyPermissions.hasPermissions(this, runtimePermission)) {
            // 已经申请过权限，做想做的事
        } else {
            // 没有申请过权限，现在去申请
            EasyPermissions.requestPermissions(this, "没有授予权限将导致某些功能异常", 2, runtimePermission);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        // 请求成功执行相应的操作
        switch (requestCode) {
            case 0:
                Toast.makeText(this, "已获取WRITE_EXTERNAL_STORAGE权限", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                Toast.makeText(this, "已获取WRITE_EXTERNAL_STORAGE和WRITE_EXTERNAL_STORAGE权限", Toast.LENGTH_SHORT).show();
                break;

            default:
                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        // 请求失败，执行相应操作
        ToastUtils.showError("请求权限失败");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

        switch (requestCode) {
            case 10011:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.installAPP(MainActivity.this);
                } else {
                    ToastUtils.showInfo("请允许该程序安装更新！");
                    Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
                    startActivityForResult(intent, 10012);
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (resultCode == 888) {
                String username = data.getStringExtra("username");
                headerBind.tvNavUsername.setText(username);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.getInstance().unSubscribe(this);
    }
}

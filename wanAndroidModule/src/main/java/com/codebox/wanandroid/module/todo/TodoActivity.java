package com.codebox.wanandroid.module.todo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.common.RouterPath;
import com.codebox.common.util.ToastUtils;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.databinding.ActivityTodoBinding;
import com.codebox.wanandroid.module.tree.TabPagerAdapter;

import java.util.ArrayList;

/**
 * @author Able
 * @date 2019/1/8
 */
//@Route(path = RouterPath.WanAndroidRouterPath.PATH_TODOLIST)
public class TodoActivity extends BaseActivity<ActivityTodoBinding> {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        initView();
        initListener();
    }

//    private String[] mTitle = {"只用这一个", "工作", "学习", "生活"};
    private String[] mTitle = {"只用这一个"};


    private void initView() {
        setTitle("TODO工具");
        setNavigationIcon(R.drawable.ic_back);

        ArrayList<Fragment> fragments = new ArrayList<>();
        ArrayList<String> titles = new ArrayList<>();


        for (int i = 0; i < mTitle.length; i++) {
            fragments.add(TodoListFragment.newInstance(i));
            titles.add(mTitle[i]);
        }

        mBinding.viewPager.setAdapter(new TabPagerAdapter(getSupportFragmentManager(), fragments, titles));

        mBinding.viewPager.setOffscreenPageLimit(mTitle.length);
        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager);
    }

    private void initListener() {

        final boolean[] isOpen = {false};

        mBinding.fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                RotateAnimation rotateAnimation = null;
                if (v.getId() == R.id.fab_add) {

                    ToastUtils.showSuccess(isOpen[0] + "");
                    if (!isOpen[0]) {
                        rotateAnimation = new RotateAnimation(0f, 45f,
                                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        mBinding.fabAdd.setAnimation(rotateAnimation);
                    } else {
                        rotateAnimation = new RotateAnimation(45f, 90f,
                                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    }
                    rotateAnimation.setDuration(200);
                    rotateAnimation.setFillAfter(true);
                    mBinding.fabAdd.setAnimation(rotateAnimation);
                    isOpen[0] = !isOpen[0];

                    TranslateAnimation translateAnimation = new TranslateAnimation(0, 100, 0, 100);
                    mBinding.llOperator.setAnimation(translateAnimation);
                }
            }
        });


    }
}

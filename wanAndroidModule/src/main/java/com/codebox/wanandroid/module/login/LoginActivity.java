package com.codebox.wanandroid.module.login;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.base.EventMsg;
import com.codebox.common.base.Resource;
import com.codebox.common.base.Status;
import com.codebox.common.common.RouterPath;
import com.codebox.common.http.HttpParamMap;
import com.codebox.common.util.LogUtils;
import com.codebox.common.util.RxBus;
import com.codebox.common.util.StatusBarUtils;
import com.codebox.common.util.ToastUtils;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.UserBean;
import com.codebox.wanandroid.databinding.ActivityLoginBinding;
import com.codebox.wanandroid.util.SpHelper;

import java.util.HashMap;


/**
 * @author Able
 * @date 2018/12/10
 */
@Route(path = RouterPath.WanAndroidRouterPath.PATH_LOGIN)
public class LoginActivity extends BaseActivity<ActivityLoginBinding> implements View.OnClickListener {

    private LoginViewModel mLoginViewModel;
    private boolean isRegister = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();
        initListener();
    }

    private void initView() {
        StatusBarUtils.setFullScreen(this);
        getSupportActionBar().hide();
        setTitle("登录");
        setNavigationIcon(R.drawable.ic_back);

    }

    public void initListener() {
        mBinding.btnLogin.setOnClickListener(this);
        mBinding.tvRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_login) {
            if (isRegister) {
                reqRegister();
            } else {
                reqLogin();
            }

        }
        if (v.getId() == R.id.tv_register) {
            isRegister = !isRegister;
            switchLoginOrRegister(isRegister);
        }
    }


    private void switchLoginOrRegister(boolean isRegister) {
        if (isRegister) {
            mBinding.rlRePassword.setVisibility(View.VISIBLE);
            mBinding.tvRegister.setText("已有账号？去登陆");
            mBinding.btnLogin.setText("注册");
        } else {
            mBinding.rlRePassword.setVisibility(View.INVISIBLE);
            mBinding.tvRegister.setText("还没有账号？去注册");
            mBinding.btnLogin.setText("登录");
        }
    }

    private void reqLogin() {
        mLoginViewModel = ViewModelProviders.of(LoginActivity.this).get(LoginViewModel.class);
        String username = mBinding.etUsername.getText().toString();
        String password = mBinding.etPassword.getText().toString();
        HashMap hashMap = new HashMap<String, Object>();
        hashMap.put("username", username);
        hashMap.put("password", password);

        mLoginViewModel.loginApp(hashMap).observe(LoginActivity.this, new Observer<Resource<UserBean>>() {
            @Override
            public void onChanged(@Nullable Resource<UserBean> userBeanResource) {
                if (userBeanResource != null && userBeanResource.status == Status.SUCCESS) {

                    String username = userBeanResource.data.getUsername();
                    EventMsg eventMsg = new EventMsg<UserBean>();
                    eventMsg.setCode(100);
                    eventMsg.setMsg(username);
                    eventMsg.setData(userBeanResource.data);
                    RxBus.getInstance().post(eventMsg);

                    SpHelper.setUsername(username);

                    LoginActivity.this.finish();
                } else {
                    ToastUtils.showInfo("登录失败，请检查账号密码");
                }
//                getTodo();

            }
        });
    }


    private void reqRegister() {
        String username = mBinding.etUsername.getText().toString();
        String password = mBinding.etPassword.getText().toString();
        String rePassword = mBinding.etRePassword.getText().toString();

        HttpParamMap hashMap = new HttpParamMap();
        if (!TextUtils.isEmpty(username)) {
            hashMap.put("username", username);
        }
        if (!TextUtils.isEmpty(password)) {
            hashMap.put("password", password);
        }
        if (!TextUtils.isEmpty(rePassword)) {
            hashMap.put("repassword", rePassword);
        }
        mLoginViewModel = ViewModelProviders.of(LoginActivity.this).get(LoginViewModel.class);
        mLoginViewModel.registerApp(hashMap).observe(LoginActivity.this, new Observer<Resource<UserBean>>() {
            @Override
            public void onChanged(@Nullable Resource<UserBean> userBeanResource) {
                if (userBeanResource.status == Status.SUCCESS) {
                    ToastUtils.showInfo("注册成功");
                    LoginActivity.this.reqLogin();
                    LogUtils.e(userBeanResource.data.toString());
                } else {
                    ToastUtils.showInfo("注册失败，请重试" + userBeanResource.status);
                }
            }
        });
//        mLoginViewModel.registerAndLogin(hashMap).observe(LoginActivity.this, userBeanResource -> {
//            if (userBeanResource != null && userBeanResource.status == Status.SUCCESS) {
//                ToastUtils.showInfo("注册成功");
//                LogUtils.e(userBeanResource.data.toString());
//            } else {
//                ToastUtils.showInfo("注册失败，请重试" + userBeanResource.status);
//            }
//        });
    }

}

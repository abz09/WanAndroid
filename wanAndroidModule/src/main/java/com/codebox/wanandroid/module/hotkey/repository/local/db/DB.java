package com.codebox.wanandroid.module.hotkey.repository.local.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.codebox.wanandroid.bean.HotKeyBean;
import com.codebox.wanandroid.module.hotkey.repository.local.dao.HotKeyDao;

/**
 *
 */
@Database(entities = {HotKeyBean.class}, version = 1, exportSchema = false)
public abstract class DB extends RoomDatabase {
    public abstract HotKeyDao getHotKeyDao();

}

package com.codebox.wanandroid.module.todo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.common.RouterPath;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.databinding.ActivityTodoListBinding;

/**
 * @author Able
 * @date 2019/3/27
 */

@Route(path = RouterPath.WanAndroidRouterPath.PATH_TODOLIST)
public class TodoListActivity extends BaseActivity<ActivityTodoListBinding> {

    private TodoFragment mUndoFragment;
    private TodoFragment mDoneFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);

        initView();
        initListener();
    }


    private void initView() {
        setTitle("只用这一个");
        setNavigationIcon(R.drawable.ic_back);

        showFragment(0);
    }

    private void initListener() {
        mBinding.bottomNavigation.setOnNavigationItemSelectedListener(new MyOnBottomNavigationListener());
    }


    /**
     * 底部导航菜单事件监听
     */
    private class MyOnBottomNavigationListener implements BottomNavigationView.OnNavigationItemSelectedListener {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == R.id.nav_schedule) {
                showFragment(0);
                return true;
            } else if (item.getItemId() == R.id.nav_completed) {
                showFragment(1);
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 显示fragment
     *
     * @param id 导航按钮id
     */
    private void showFragment(int id) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        hindFragment(transaction);
        switch (id) {
            case 0:
                if (mUndoFragment != null) {
                    transaction.show(mUndoFragment);
                } else {
                    mUndoFragment = TodoFragment.newInstance(0);
                    transaction.add(R.id.content, mUndoFragment);
                }
                break;
            case 1:
                if (mDoneFragment != null) {
                    transaction.show(mDoneFragment);
                } else {
                    mDoneFragment = TodoFragment.newInstance(1);
                    transaction.add(R.id.content, mDoneFragment);
                }
                break;
            // 添加default
            default:
                break;
        }
        transaction.commit();
    }

    /**
     * 隐藏fragment
     *
     * @param transaction
     */
    private void hindFragment(FragmentTransaction transaction) {
        if (mUndoFragment != null) {
            transaction.hide(mUndoFragment);
        }

        if (mDoneFragment != null) {
            transaction.hide(mDoneFragment);
        }
    }

}

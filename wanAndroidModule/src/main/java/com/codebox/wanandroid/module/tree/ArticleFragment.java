package com.codebox.wanandroid.module.tree;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.codebox.common.base.BaseSmartRefreshFragment;
import com.codebox.common.base.Status;
import com.codebox.common.util.LogUtils;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.ArticleBean;
import com.codebox.wanandroid.module.home.ArticleAdapter;


/**
 * @author Able
 * @date 2018/12/10
 */
public class ArticleFragment extends BaseSmartRefreshFragment<ArticleBean.DatasBean> {

    private static final String CID = "CID";
    private int cid;
    private int mPageNo = 0;

    private TreeViewModel mTreeViewModel;


    public static ArticleFragment newInstance(int id) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(CID, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        onRefresh();
    }

    private void initView() {
        cid = getArguments().getInt(CID);
        LogUtils.e(cid + "cccccccc");

        mTreeViewModel = ViewModelProviders.of(ArticleFragment.this).get(TreeViewModel.class);
        setAdapter(new ArticleAdapter(R.layout.item_home_list, mList));
    }

    @Override
    protected boolean needLoadMore() {
        return false;
    }

    @Override
    protected boolean needDividerDecoration() {
        return false;
    }

    @Override
    protected void getData() {
        mTreeViewModel.getArticleTree(mPageNo, cid).observe(ArticleFragment.this, articleBeanResource -> {
            Status status = articleBeanResource.status;

            if (status == Status.SUCCESS) {
                ArticleBean data = articleBeanResource.data;
                // 这里需要对data判空，否则data.getDatas() 会抛空指针异常
                if (data == null) {
                    dealWithData(null, status);
                } else {
                    dealWithData(data.getDatas(), status);
                }
            }

            if (status == Status.ERROR || status == Status.FAILURE) {
                dealWithData(null, status);
            }
        });
    }
}

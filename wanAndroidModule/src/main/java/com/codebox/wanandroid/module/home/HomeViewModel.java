package com.codebox.wanandroid.module.home;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.codebox.wanandroid.bean.ArticleBean;
import com.codebox.wanandroid.bean.BannerBean;
import com.codebox.wanandroid.http.HttpClient;
import com.codebox.common.base.Resource;

import java.util.List;

/**
 * @author Able
 * @date 2018/3/20
 */
public class HomeViewModel extends ViewModel {
    private HttpClient httpClient = HttpClient.getInstance();
    private LiveData<Resource<ArticleBean>> articleLiveData;
    private LiveData<Resource<List<BannerBean>>> bannerLiveData;

    /**
     * 查询文章列表
     *
     * @param pageNo 分页参数
     * @return
     */
    public LiveData<Resource<ArticleBean>> queryArticle(int pageNo) {
        // TODO 重要提示：不可复用LiveData，每次都要重新赋值，否则数据变化，不可感知
        articleLiveData = httpClient.queryArticle(pageNo);
        return articleLiveData;
    }

    /**
     * 获取首页Banner图
     *
     * @return
     */
    public LiveData<Resource<List<BannerBean>>> getBanner() {

        if (null == bannerLiveData) {
            bannerLiveData = httpClient.getBanner();
        }
        return bannerLiveData;
    }

}

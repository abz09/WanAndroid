package com.codebox.wanandroid.module.hotkey.repository.local.service;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.codebox.wanandroid.bean.HotKeyBean;
import com.codebox.wanandroid.module.hotkey.repository.local.dao.HotKeyDao;
import com.codebox.wanandroid.module.hotkey.repository.local.db.DBHelper;
import com.codebox.common.base.Resource;
import com.codebox.common.util.LogUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class HotKeyServiceImpl implements HotKeyService {

    private static final HotKeyServiceImpl instance = new HotKeyServiceImpl();

    private HotKeyServiceImpl() {
    }

    public static HotKeyServiceImpl getInstance() {
        return instance;
    }


    private HotKeyDao mHotKeyDao = DBHelper.getInstance().getDb().getHotKeyDao();

    @Override
    public LiveData<Long> add(final HotKeyBean hotKeyBean) {
        // transfer long to LiveData<Long>
        final MutableLiveData<Long> data = new MutableLiveData<>();
//        new AsyncTask<Void, Void, Long>() {
//            @Override
//            protected Long doInBackground(Void... voids) {
//                return mHotKeyDao.add(hotKeyBean);
//            }
//
//            @Override
//            protected void onPostExecute(Long rowId) {
//                data.setValue(rowId);
//            }
//        }.execute();


        Observable.create(new ObservableOnSubscribe<Long>() {
            @Override
            public void subscribe(ObservableEmitter<Long> emitter) throws Exception {
                emitter.onNext(mHotKeyDao.add(hotKeyBean));
            }
        }).subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long aLong) throws Exception {
                data.setValue(aLong);
            }
        });

        return data;
    }

    @Override
    public LiveData<Long> addList(final List<HotKeyBean> hotKeyBeans) {
        final MutableLiveData<Long> data = new MutableLiveData<>();
//        new AsyncTask<Void, Void, Long>() {
//            @Override
//            protected Long doInBackground(Void... voids) {
//                for (HotKeyBean hotKeyBean : hotKeyBeans) {
//                    mHotKeyDao.add(hotKeyBean);
//                }
//                return 1L;
//            }
//
//            @Override
//            protected void onPostExecute(Long rowId) {
//                data.setValue(rowId);
//            }
//        }.execute();


        Observable.create(new ObservableOnSubscribe<Long>() {
            @Override
            public void subscribe(ObservableEmitter<Long> emitter) throws Exception {
                emitter.onNext(addHotKeys(hotKeyBeans));
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Long aLong) {
                        data.setValue(aLong);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


        return data;
    }

    private Long addHotKeys(List<HotKeyBean> hotKeyBeans) {
        for (HotKeyBean hotKeyBean : hotKeyBeans) {
            mHotKeyDao.add(hotKeyBean);
        }
        return 1L;
    }

    @Override
    public LiveData<Resource<List<HotKeyBean>>> queryAll() {
        final MutableLiveData liveData = new MutableLiveData();

//        new AsyncTask<Void, Void, List<HotKeyBean>>() {
//            @Override
//            protected List<HotKeyBean> doInBackground(Void... voids) {
//                return mHotKeyDao.queryAll();
//            }
//
//            @Override
//            protected void onPostExecute(List<HotKeyBean> hotKeyBeans) {
//                liveData.setValue(Resource.success(hotKeyBeans));
//            }
//        }.execute();

//        List<HotKeyBean> data = mHotKeyDao.queryAll();
//        liveData.setValue(Resource.success(data));


        Observable
                .create(new ObservableOnSubscribe<List<HotKeyBean>>() {

                    @Override
                    public void subscribe(ObservableEmitter emitter) throws Exception {
                        emitter.onNext(getHotKey());
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<HotKeyBean>>() {
                    @Override
                    public void accept(List<HotKeyBean> hotKeyBeans) throws Exception {
                        liveData.setValue(Resource.success(hotKeyBeans));
                    }
                });


//        new Observer<List<HotKeyBean>>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//
//                    }
//
//                    @Override
//                    public void onNext(List<HotKeyBean> hotKeyBeans) {
//                        LogUtils.d("onNext thread is :" + Thread.currentThread().getName());
//                        liveData.setValue(Resource.success(hotKeyBeans));
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });

        return liveData;
    }

    private List<HotKeyBean> getHotKey() {
        LogUtils.d("getHotKey thread is :" + Thread.currentThread().getName());
        return mHotKeyDao.queryAll();
    }

}

package com.codebox.wanandroid.module.todo;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.codebox.common.base.BaseFragment;
import com.codebox.common.base.Resource;
import com.codebox.common.util.ToastUtils;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.TodoBean;
import com.codebox.wanandroid.bean.TodoEntity;
import com.codebox.wanandroid.databinding.FragmentTodoBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Able
 * @date 2019/3/29
 */
public class TodoFragment extends BaseFragment<FragmentTodoBinding> implements SwipeRefreshLayout.OnRefreshListener {

    public static final String STATUS = "STATUS";
    private TodoViewModel mTodoViewModel;
    private int mPageNo = 1;
    private int mStatus;
    private ArrayList<TodoEntity> mTodoEntityList;
    private TodoAdapter mTodoAdapter;

    public static TodoFragment newInstance(int status) {
        TodoFragment fragment = new TodoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(STATUS, status);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_todo;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mStatus = getArguments().getInt(STATUS);
        initView();
        onRefresh();
    }

    private void initView() {

        mBinding.swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        mBinding.swipeRefreshLayout.setOnRefreshListener(this);

        initAdapter();
    }

    private void initAdapter() {
        mTodoEntityList = new ArrayList<>();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mTodoAdapter = new TodoAdapter(R.layout.item_todo_list, R.layout.item_sticky_header, mTodoEntityList);
        mBinding.recyclerView.setAdapter(mTodoAdapter);

        mTodoAdapter.setOnItemClickListener((adapter, view, position) -> {
            ToastUtils.showInfo(position + "");
        });
    }

    @Override
    public void onRefresh() {
        mTodoViewModel = ViewModelProviders.of(TodoFragment.this).get(TodoViewModel.class);
        mTodoViewModel
                .getTodo(1, mStatus)
                .observe(TodoFragment.this, new Observer<Resource<TodoBean>>() {
                    @Override
                    public void onChanged(@Nullable Resource<TodoBean> todoBeanResource) {
                        dealData(todoBeanResource);
                    }
                });
    }

    private void dealData(Resource<TodoBean> todoBeanResource) {
        mBinding.swipeRefreshLayout.setRefreshing(false);
        switch (todoBeanResource.status) {
            case SUCCESS:
                List<TodoBean.DatasBean> datas = todoBeanResource.data.getDatas();
                mTodoEntityList.clear();
                long dateIndex = 0;
                for (TodoBean.DatasBean d : datas) {
                    if (dateIndex == d.getDate()) {
                        TodoEntity todoEntity = new TodoEntity(d);
                        mTodoEntityList.add(todoEntity);
                    } else {
                        dateIndex = d.getDate();
                        TodoEntity todoHead = new TodoEntity(true, d.getDateStr());
                        mTodoEntityList.add(todoHead);
                        TodoEntity todoEntity = new TodoEntity(d);
                        mTodoEntityList.add(todoEntity);
                    }
                }
                mTodoAdapter.notifyDataSetChanged();
                break;

            case LOADING:
                break;

            case ERROR:
                break;

            case FAILURE:
                ToastUtils.showError(todoBeanResource.message);
                break;

            default:
                break;
        }
    }
}

package com.codebox.wanandroid.module.home.rxmerge;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.codebox.common.base.BaseSmartRefreshFragment;
import com.codebox.common.base.Resource;
import com.codebox.common.base.Status;
import com.codebox.common.common.WebActivity;
import com.codebox.common.imageloader.GlideImageLoader;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.ArticleBean;
import com.codebox.wanandroid.bean.BannerBean;
import com.codebox.wanandroid.common.Constants;
import com.codebox.wanandroid.module.home.ArticleAdapter;
import com.youth.banner.Banner;

import java.util.ArrayList;
import java.util.List;

/**
 * 使用rxjava2来实现homeFragment 的多个接口请求
 *
 * @author 李立
 * @date 2018/12/31
 */
public class HomeFragmentWithRxMerge extends BaseSmartRefreshFragment<ArticleBean.DatasBean> {

    private HomeViewModelRxMerge mHomeViewModel;

    private ArticleAdapter mArticleAdapter;

    private List<BannerBean> mBannerList;
    private ArrayList<String> mImagePaths;
    private ArrayList<String> mBannerTitles;

    private Banner mBanner;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        onRefresh();
    }

    private void initView() {
        mImagePaths = new ArrayList<>();
        mBannerTitles = new ArrayList<>();

        mHomeViewModel = ViewModelProviders.of(HomeFragmentWithRxMerge.this).get(HomeViewModelRxMerge.class);
        mArticleAdapter = new ArticleAdapter(R.layout.item_home_list, mList);
        setAdapter(mArticleAdapter);

        View view = getActivity().getLayoutInflater().inflate(R.layout.item_banner, null, false);
        mBanner = view.findViewById(R.id.banner);
        mArticleAdapter.addHeaderView(view);

        //监听文章列表数据和banner数据变化的回调
        mHomeViewModel.getBannerLiveData().observe(HomeFragmentWithRxMerge.this, this::dealBanner);
        mHomeViewModel.getArticleData() .observe(HomeFragmentWithRxMerge.this, this::dealArticleList);

    }

    @Override
    protected boolean needDividerDecoration() {
        return true;
    }

    /**
     * 刷新网络请求
     */
    @Override
    protected void getData() {
        mHomeViewModel.getArticleAndBannerWithMerge(mPageNo);
    }

    private void setBannerAdapter() {
        mBanner
                .setImages(mImagePaths)
                .setImageLoader(new GlideImageLoader())
                .setBannerTitles(mBannerTitles)
                .setOnBannerListener(position -> {} )
                .start();
    }


    /**
     * 处理文章列表ui。
     */
    private void dealArticleList(Resource<ArticleBean> articleBeanResource) {
        Status status = articleBeanResource.status;

        if (status == Status.SUCCESS) {
            ArticleBean data = articleBeanResource.data;
            if (data == null) {
                HomeFragmentWithRxMerge.this.dealWithData(null, status);
            } else {
                HomeFragmentWithRxMerge.this.dealWithData(data.getDatas(), status);
            }
        }

        if (status == Status.ERROR || status == Status.FAILURE) {
            HomeFragmentWithRxMerge.this.dealWithData(null, status);
        }
    }

    /**
     * 设置banner 数据
     */
    private void dealBanner(Resource<List<BannerBean>> listResource) {
        switch (listResource.status) {
            case SUCCESS:
                mBannerList = listResource.data;
                mImagePaths.clear();
                mBannerTitles.clear();
                for (int i = 1; i < mBannerList.size(); i++) {
                    mImagePaths.add(mBannerList.get(i).getImagePath());
                    mBannerTitles.add(mBannerList.get(i).getTitle());
                }
                setBannerAdapter();

                break;

            case LOADING:
                break;

            case ERROR:
                break;

            case FAILURE:
                break;

            default:
                break;
        }

    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.onItemClick(adapter, view, position);
        ArticleBean.DatasBean article = mList.get(position);
        goWebView(article.getTitle(), article.getLink());
    }

    private void goWebView(String title, String link) {
        Intent intent = new Intent(getContext(), WebActivity.class);
        if (!TextUtils.isEmpty(title)) {
            intent.putExtra(Constants.EXTRA_WEB_TITLE, title);
        }
        if (!TextUtils.isEmpty(link)) {
            intent.putExtra(Constants.EXTRA_WEB_URL, link);
        }
        startActivity(intent);
    }

}

package com.codebox.wanandroid.module.hotkey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.HotKeyBean;
import com.codebox.common.util.RandomUtils;
import com.codebox.wanandroid.common.Constants;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;

import java.util.List;


/**
 * @author Jenly <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
public class HotKeyTagAdapter extends TagAdapter<HotKeyBean> {
    private Context mContext;
    private LayoutInflater mInflater;

    public HotKeyTagAdapter(Context context, List<HotKeyBean> datas) {
        super(datas);
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(FlowLayout parent, int position, HotKeyBean data) {
        TextView tv = (TextView) mInflater.inflate(R.layout.item_hot_key, parent, false);
        tv.setText(data.getName());
        tv.setTextColor(RandomUtils.INSTANCE.randomColor(Constants.COLOR_RGB_MIN, Constants.COLOR_RGB_MAX));
        return tv;
    }

}

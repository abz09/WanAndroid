package com.codebox.wanandroid.module.hotkey;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.codebox.wanandroid.bean.HotKeyBean;
import com.codebox.wanandroid.http.HttpClient;
import com.codebox.wanandroid.module.hotkey.repository.HotKeyRepository;
import com.codebox.common.base.Resource;

import java.util.List;

public class HotKeyViewModel extends ViewModel {

    private HttpClient httpClient = HttpClient.getInstance();
    private HotKeyRepository hotKeyRepository = HotKeyRepository.getInstance();

    /**
     * 查询热词
     *
     * @return
     */
    public LiveData<Resource<List<HotKeyBean>>> getHotKey() {
//        return httpClient.getHotKey();

        return hotKeyRepository.getHotKey();
    }
}

package com.codebox.wanandroid.module.hotkey.repository;

import android.arch.lifecycle.LiveData;

import com.codebox.wanandroid.bean.HotKeyBean;
import com.codebox.common.base.Resource;

import java.util.List;

public interface HotKeyDataSource {
    LiveData<Resource<List<HotKeyBean>>> queryAll();
}

package com.codebox.wanandroid.module.test;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.common.RouterPath;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.databinding.ActivityMyTestBinding;


/**
 * @author Able
 * @date 2018/12/28
 */
@Route(path = RouterPath.WanAndroidRouterPath.PATH_TEST2)
public class TestActivity extends AppCompatActivity {

    protected ActivityMyTestBinding mBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_test);
        initView();
        initData();
    }


    private void initView() {
    }

    private void initData() {
    }

}

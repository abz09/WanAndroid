package com.codebox.wanandroid.module.hotkey.repository.local.service;

import android.arch.lifecycle.LiveData;

import com.codebox.wanandroid.bean.HotKeyBean;
import com.codebox.common.base.Resource;

import java.util.List;

public interface HotKeyService {
    LiveData<Long> add(HotKeyBean hotKeyBean);

    LiveData<Long> addList(List<HotKeyBean> hotKeyBeans);

    LiveData<Resource<List<HotKeyBean>>> queryAll();
}

package com.codebox.wanandroid.module.about;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.common.RouterPath;
import com.codebox.common.util.StatusBarUtils;
import com.codebox.common.util.ToastUtils;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.databinding.ActivityAboutBinding;


/**
 * @author Able
 * @date 2018/8/29
 */
@Route(path = RouterPath.WanAndroidRouterPath.PATH_ABOUT_US)
public class AboutActivity extends AppCompatActivity {

    @Autowired
    public long order;
    @Autowired
    public String name;
    @Autowired
    int age;

    private ImageView ivAbout;
    private CollapsingToolbarLayout collapsingToolbar;
    private TextView tv;
    private Toolbar mToolbar;
    private ActivityAboutBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_about);

        getIntentData();
        getBundleData();

        initView();
        initData();

    }

    /**
     * 通过路由直接发送bundle通过普通方式拿到bundle
     */
    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();

        if (bundle == null) {
            ToastUtils.showToast("bundle为空");
            return;
        }

        String key1 = bundle.getString("bundleKey1");
        String key2 = bundle.getString("bundleKey2");
        String key3 = bundle.getString("bundleKey3");
        String key4 = bundle.getString("bundleKey4");

        ToastUtils.showToast("key1:" + key1 + "key2:" + key2 + "key3:" + key3 + "key4:" + key4);
    }

    /**
     * 直接传参数过来可以通过路由设置
     */
    private void getIntentData() {
//        ARouter.getInstance().inject(this);
//        ToastUtils.showToast("order:"+order +"age:"+age + "name:"+name + "boy:"+boy);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);

    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar_a);
        mToolbar.setNavigationOnClickListener(view -> finish());
        StatusBarUtils.setTranslucentImageHeader(this, 0, mToolbar);
        // 设置显示menu
        setSupportActionBar(mBinding.toolbarA);
        getSupportActionBar().setHomeButtonEnabled(true);



        ivAbout = findViewById(R.id.ivAbout);
        collapsingToolbar = findViewById(R.id.collapsingToolbar);
        tv = findViewById(R.id.tv);

    }

    private void initData() {
        ivAbout.setImageResource(R.mipmap.ic_about_image);

        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(this, R.color.white));
        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.white));
        collapsingToolbar.setCollapsedTitleGravity(Gravity.START);
        tv.setText(Html.fromHtml(getString(R.string.about_desc)));
        mBinding.tv.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuGithub) {
            ToastUtils.showInfo("menuGithub");

        } else if (id == R.id.menuSource) {
            ToastUtils.showInfo("menuSource");

        } else if (id == R.id.menuIssue) {
            ToastUtils.showInfo("menuIssue");

        } else if (id == R.id.menuEmail) {
            ToastUtils.showInfo("menuEmail");

        } else if (id == R.id.menuQQGroup) {
            ToastUtils.showInfo("menuQQGroup");

        } else {
        }
        return super.onOptionsItemSelected(item);
    }
}

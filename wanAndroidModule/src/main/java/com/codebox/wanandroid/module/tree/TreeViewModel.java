package com.codebox.wanandroid.module.tree;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.codebox.wanandroid.bean.ArticleBean;
import com.codebox.common.base.Resource;
import com.codebox.wanandroid.bean.TreeBean;
import com.codebox.wanandroid.http.HttpClient;

import java.util.List;

/**
 * @author Able
 * @date 2018/10/25
 */
public class TreeViewModel extends ViewModel {
    private HttpClient httpClient = HttpClient.getInstance();
    private LiveData<Resource<List<TreeBean>>> treeLiveData;
    private LiveData<Resource<ArticleBean>> articleTree;


    /**
     * 查询体系
     *
     * @return
     */
    public LiveData<Resource<List<TreeBean>>> getTreeData() {

        treeLiveData = httpClient.getTreeData();
        return treeLiveData;
    }

    public LiveData<Resource<ArticleBean>> getArticleTree(int pageNo, int cid) {

        articleTree = httpClient.getArticleTree(pageNo, cid);
        return articleTree;
    }


}

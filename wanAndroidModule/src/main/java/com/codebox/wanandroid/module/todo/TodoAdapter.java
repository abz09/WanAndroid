package com.codebox.wanandroid.module.todo;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.TodoBean;
import com.codebox.wanandroid.bean.TodoEntity;

import java.util.List;

/**
 * @author Able
 * @date 2019/1/8
 */
public class TodoAdapter extends BaseSectionQuickAdapter<TodoEntity, BaseViewHolder> {

    public TodoAdapter(int layoutResId, int sectionHeadResId, List<TodoEntity> data) {
        super(layoutResId, sectionHeadResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TodoEntity entity) {
        TodoBean.DatasBean item = entity.t;
        helper
                .setText(R.id.tv_title, item.getTitle())
                .setText(R.id.tv_content, item.getContent())
                .addOnClickListener(R.id.btn_delete)
                .addOnClickListener(R.id.btn_done)
                .addOnClickListener(R.id.item_todo_content);

        if (item.getStatus() == 0) {
            helper.setText(R.id.btn_done, "标记为完成");
        } else if (item.getStatus() == 1) {
            helper.setText(R.id.btn_done, "复原");
        }
    }

    @Override
    protected void convertHead(BaseViewHolder helper, TodoEntity item) {
        helper.setText(R.id.tv_header, item.header);
    }
}

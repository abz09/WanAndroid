package com.codebox.wanandroid.module.login;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.codebox.common.base.Resource;
import com.codebox.wanandroid.bean.UserBean;
import com.codebox.wanandroid.http.HttpClient;

import java.util.HashMap;

/**
 * @author Able
 * @date 2018/12/10
 */
public class LoginViewModel extends ViewModel {
    private HttpClient mHttpClient = HttpClient.getInstance();

    /**
     * 登录
     *
     * @param map
     * @return
     */
    public LiveData<Resource<UserBean>> loginApp(HashMap map) {
        return mHttpClient.loginApp(map);
    }

    /**
     * 注册
     *
     * @param hashMap 注册参数map
     */
    public LiveData<Resource<UserBean>> registerApp(HashMap hashMap) {
        return mHttpClient.registerApp(hashMap);
    }


    public LiveData<Resource<UserBean>> registerAndLogin(HashMap hashMap) {
        return mHttpClient.registerAndLogin(hashMap);
    }

}

package com.codebox.wanandroid.module.home.rxmerge;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.codebox.common.base.BaseResult;
import com.codebox.common.base.Resource;
import com.codebox.common.http.RetrofitFactory;
import com.codebox.wanandroid.bean.ArticleBean;
import com.codebox.wanandroid.bean.BannerBean;
import com.codebox.wanandroid.http.RetrofitApi;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 使用Rxjava2 的方法和LiveData 结合viewmodel
 *
 * @author 李立
 * @date 2018/12/31
 *
 */
public class HomeViewModelRxMerge extends ViewModel {

    private RetrofitApi mApi = RetrofitFactory.getInstance().create(RetrofitApi.class);

    private MutableLiveData<Resource<ArticleBean>> mArticleLiveData;
    private MutableLiveData<Resource<List<BannerBean>>> mBannerLiveData;

    public HomeViewModelRxMerge(){
        mArticleLiveData = new MutableLiveData<>();
        mBannerLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<Resource<List<BannerBean>>> getBannerLiveData(){
        return mBannerLiveData;
    }

    public MutableLiveData<Resource<ArticleBean>> getArticleData(){
        return mArticleLiveData;
    }

    /**
     * 使用Rxjava2 merge方式请求两个接口
     */
    public void getArticleAndBannerWithMerge(int pageNo){
        mArticleLiveData.setValue(Resource.loading());

        Observable<BaseResult<ArticleBean>> observableArticle = mApi.queryArticle(pageNo);
        Observable<BaseResult<List<BannerBean>>> observableBanner = mApi.getBanner();

        if(pageNo < 1){
            mBannerLiveData.setValue(Resource.loading());
            Observable.merge(observableArticle, observableBanner)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    //如果这里不关心onError的回调也可以使用Consumer 只关心onNext方法
                    .subscribe(new Observer<BaseResult<? extends Object>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(BaseResult<?> baseResult) {
                            Object object = baseResult.getData();
                            if (object instanceof ArticleBean) {
                                BaseResult<ArticleBean> t1 = (BaseResult<ArticleBean>) baseResult;
                                mArticleLiveData.setValue(Resource.response(t1));
                            } else {
                                if(object instanceof List){
                                    List list = ((List)object);

                                    if(list.size() > 0){
                                        Object object2 = list.get(0);
                                        if(object2 instanceof BannerBean ){
                                            BaseResult<List<BannerBean>> t2 = (BaseResult<List<BannerBean>>) baseResult;
                                            mBannerLiveData.setValue(Resource.response(t2));
                                        }
                                    }
                                }
                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            mArticleLiveData.setValue(Resource.error(e));
                            mBannerLiveData.setValue(Resource.error(e));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }else{
            mApi.queryArticle(pageNo)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    //如果这里不关心onError的回调也可以使用Consumer 只关心onNext方法
                    .subscribe(new Observer<BaseResult<ArticleBean>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(BaseResult<ArticleBean> articleBeanBaseResult) {
                            mArticleLiveData.setValue(Resource.response(articleBeanBaseResult));
                        }

                        @Override
                        public void onError(Throwable e) {
                            mArticleLiveData.setValue(Resource.error(e));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }
}

package com.codebox.wanandroid.module.tree;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.codebox.common.base.BaseFragment;
import com.codebox.common.base.Resource;
import com.codebox.common.common.WebActivity;
import com.codebox.common.util.ToastUtils;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.ArticleBean;
import com.codebox.wanandroid.common.Constants;
import com.codebox.wanandroid.databinding.FragmentArticleListBinding;
import com.codebox.wanandroid.module.home.ArticleAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Able
 * @date 2018/10/26
 */
public class ArticleListFragment extends BaseFragment<FragmentArticleListBinding> implements
        SwipeRefreshLayout.OnRefreshListener {

    public static String CID = "CID";
    private int cid;
    private int pageNo = 0;
    private TreeViewModel treeViewModel;
    private ArrayList<ArticleBean.DatasBean> mArticleList;
    private ArticleAdapter mArticleAdapter;

    public static ArticleListFragment newInstance(int id) {
        ArticleListFragment fragment = new ArticleListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(CID, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_article_list;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cid = getArguments().getInt(CID);
        initView();
        onRefresh();
    }

    private void initView() {

        mBinding.swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        mBinding.swipeRefreshLayout.setOnRefreshListener(this);

        initAdapter();
    }

    private void initAdapter() {
        mArticleList = new ArrayList<>();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mArticleAdapter = new ArticleAdapter(R.layout.item_home_list, mArticleList);
        mBinding.recyclerView.setAdapter(mArticleAdapter);

        mArticleAdapter.setOnItemClickListener((adapter, view, position) -> {
            ToastUtils.showInfo(position + "");
            ArticleBean.DatasBean article = mArticleList.get(position);
            goWebView(article.getTitle(), article.getLink());
        });
    }

    private void goWebView(String title, String link) {
        Intent intent = new Intent(getContext(), WebActivity.class);
        intent.putExtra(Constants.EXTRA_WEB_TITLE, title);
        intent.putExtra(Constants.EXTRA_WEB_URL, link);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        treeViewModel = ViewModelProviders.of(ArticleListFragment.this).get(TreeViewModel.class);
        treeViewModel
                .getArticleTree(pageNo, cid)
                .observe(ArticleListFragment.this, this::dealArticle);
    }

    private void dealArticle(Resource<ArticleBean> articleBeanResource) {
        switch (articleBeanResource.status) {
            case SUCCESS:
                List<ArticleBean.DatasBean> datas = articleBeanResource.data.getDatas();
                mArticleList.clear();
                mArticleList.addAll(datas);
                mArticleAdapter.notifyDataSetChanged();
                mBinding.swipeRefreshLayout.setRefreshing(false);
                break;

            case LOADING:
                break;

            case ERROR:
                break;

            case FAILURE:
                break;

            default:
                break;
        }
    }
}

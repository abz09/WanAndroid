package com.codebox.wanandroid.module.tree;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.codebox.wanandroid.R;
import com.codebox.wanandroid.common.Constants;
import com.codebox.wanandroid.bean.TreeBean;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.codebox.common.base.BaseSmartRefreshFragment;
import com.codebox.common.base.Status;
import com.codebox.common.util.GsonUtils;
import com.codebox.common.util.ToastUtils;


/**
 * 知识体系fragment
 *
 * @author
 * @date
 */
public class TreeFragment extends BaseSmartRefreshFragment<TreeBean> {

    private TreeViewModel mTreeViewModel;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        onRefresh();
    }

    @Override
    protected boolean needLoadMore() {
        return false;
    }

    private void initView() {
        mTreeViewModel = ViewModelProviders.of(TreeFragment.this).get(TreeViewModel.class);

        TreeAdapter treeAdapter = new TreeAdapter(R.layout.item_tree_list, mList);
        setAdapter(treeAdapter);
    }

    @Override
    protected void getData() {
        mTreeViewModel.getTreeData().observe(TreeFragment.this, listResource -> {
            Status status = listResource.status;
            dealWithData(listResource.data, status);
        });
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        super.onItemClick(adapter, view, position);
        ToastUtils.showInfo(position + "");

        TreeBean treeBean = (TreeBean) mList.get(position);
        Intent intent = new Intent(getContext(), TreeArticleActivity.class);
        intent.putExtra(Constants.EXTRA_TREE_NAME, treeBean.getName());
        intent.putExtra(Constants.EXTRA_TREE_CHILDREN, GsonUtils.toJson(treeBean.getChildren()));
        startActivity(intent);
    }
}

package com.codebox.wanandroid.module.home;

import android.support.annotation.Nullable;

import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.ArticleBean;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.codebox.common.util.UIUtil;

import java.util.List;

/**
 * @author Administrator
 * @date 2017/12/12
 */
public class ArticleAdapter extends BaseQuickAdapter<ArticleBean.DatasBean, BaseViewHolder> {
    public ArticleAdapter(int layoutResId, @Nullable List<ArticleBean.DatasBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ArticleBean.DatasBean item) {

        helper.setText(R.id.tv_title, item.getTitle())
                .setText(R.id.tv_author, item.getAuthor())
                .setText(R.id.tv_type, item.getChapterName())
                .addOnClickListener(R.id.tv_type)
                .setTextColor(R.id.tv_type, UIUtil.getColor(R.color.colorPrimary))
                .linkify(R.id.tv_type)
                .setText(R.id.tv_date, item.getNiceDate())
                .setImageResource(R.id.iv_like, R.drawable.ic_action_no_like)
                .addOnClickListener(R.id.iv_like);
    }
}

package com.codebox.wanandroid.module.hotkey.repository.local;

import android.arch.lifecycle.LiveData;

import com.codebox.wanandroid.bean.HotKeyBean;
import com.codebox.wanandroid.module.hotkey.repository.HotKeyDataSource;
import com.codebox.wanandroid.module.hotkey.repository.local.service.HotKeyService;
import com.codebox.wanandroid.module.hotkey.repository.local.service.HotKeyServiceImpl;
import com.codebox.common.base.Resource;

import java.util.List;

/**
 * Created by 86839 on 2017/10/6.
 */

public class LocalHotKeyDataSource implements HotKeyDataSource {
    private static final LocalHotKeyDataSource instance = new LocalHotKeyDataSource();

    private LocalHotKeyDataSource() {
    }

    public static LocalHotKeyDataSource getInstance() {
        return instance;
    }


    private HotKeyService mHotKeyService = HotKeyServiceImpl.getInstance();

    @Override
    public LiveData<Resource<List<HotKeyBean>>> queryAll() {
        return mHotKeyService.queryAll();
    }

    public LiveData<Long> add(HotKeyBean hotKeyBean) {
        return mHotKeyService.add(hotKeyBean);
    }

    public LiveData<Long> addList(List<HotKeyBean> hotKeyBeans) {
        return mHotKeyService.addList(hotKeyBeans);
    }

}

package com.codebox.wanandroid.module.hotkey.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.codebox.wanandroid.bean.HotKeyBean;

import java.util.List;

@Dao
public interface HotKeyDao {
    // cache need update
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long add(HotKeyBean hotKeyBean);

    @Query("select * from HOTKEY")
    List<HotKeyBean> queryAll();
}

package com.codebox.wanandroid.module.tree;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.TreeBean;

import java.util.List;

/**
 * @author Able
 * @date 2018/10/25
 */
public class TreeAdapter extends BaseQuickAdapter<TreeBean, BaseViewHolder> {
    public TreeAdapter(int layoutResId, @Nullable List<TreeBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TreeBean item) {
        String childrenNames = "";
        for (TreeBean.ChildrenBean child : item.getChildren()) {
            childrenNames += child.getName() + "   ";
        }
        helper
                .setText(R.id.tv_tree_1, item.getName())
                .setText(R.id.tv_tree_2, childrenNames);
    }
}

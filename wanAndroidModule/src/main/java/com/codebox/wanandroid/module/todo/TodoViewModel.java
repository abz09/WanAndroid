package com.codebox.wanandroid.module.todo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.codebox.common.base.Resource;
import com.codebox.wanandroid.bean.TodoBean;
import com.codebox.wanandroid.http.HttpClient;

/**
 * @author Able
 * @date 2019/1/8
 */
public class TodoViewModel extends ViewModel {
    private HttpClient httpClient = HttpClient.getInstance();

    public LiveData<Resource<TodoBean>> getTodo(int pageNo, int status) {
        return httpClient.getTodo(pageNo, status);
    }
}

package com.codebox.wanandroid.module.tree;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.common.RouterPath;
import com.codebox.common.util.GsonUtils;
import com.codebox.common.util.LogUtils;
import com.codebox.wanandroid.R;
import com.codebox.wanandroid.bean.TreeBean;
import com.codebox.wanandroid.common.Constants;
import com.codebox.wanandroid.databinding.ActivityTreeArticleBinding;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Able
 * @date 2018/10/26
 */
@Route(path = RouterPath.WanAndroidRouterPath.PATH_TREE_ARTICLE)
public class TreeArticleActivity extends BaseActivity<ActivityTreeArticleBinding> {

    private String mTitle;
    private String childrenJson;
    private List<TreeBean.ChildrenBean> childrenBeans;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tree_article);

        getExtra();
        initView();
    }

    private void getExtra() {
        mTitle = getIntent().getStringExtra(Constants.EXTRA_TREE_NAME);
        childrenJson = getIntent().getStringExtra(Constants.EXTRA_TREE_CHILDREN);

        childrenBeans = GsonUtils.getObjs(childrenJson, new TypeToken<List<TreeBean.ChildrenBean>>() {
        });

        LogUtils.e(childrenBeans.get(0).getName());
    }

    private void initView() {
        setNavigationIcon(R.drawable.ic_back);
        setTitle(mTitle);

        ArrayList<Fragment> fragments = new ArrayList<>();
        ArrayList<String> titles = new ArrayList<>();
        for (int i = 0; i < childrenBeans.size(); i++) {
//            fragments.add(ArticleListFragment.newInstance(childrenBeans.get(i).getId()));
            fragments.add(ArticleFragment.newInstance(childrenBeans.get(i).getId()));
            titles.add(childrenBeans.get(i).getName());
        }

        mBinding.viewPager.setAdapter(new TabPagerAdapter(getSupportFragmentManager(), fragments, titles));

        mBinding.viewPager.setOffscreenPageLimit(childrenBeans.size());
        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager);
    }
}

package com.codebox.wanandroid.common;


/**
 * @author Able
 * @date 2018/3/9
 * @description 常量定义类
 */
public class Constants {

    public static final int COLOR_RGB_MIN = 20;
    public static final int COLOR_RGB_MAX = 230;
    public static final String EXTRA_WEB_TITLE = "EXTRA_WEB_TITLE";
    public static final String EXTRA_WEB_URL = "EXTRA_WEB_URL";
    public static final String EXTRA_TREE_NAME = "EXTRA_TREE_NAME";
    public static final String EXTRA_TREE_CHILDREN = "EXTRA_TREE_CHILDREN";
    public static String USERNAME = "USERNAME";
}

package com.codebox.wanandroid.util;

import android.content.Context;

import com.codebox.common.common.CommonModule;
import com.codebox.common.util.SPUtil;
import com.codebox.wanandroid.common.Constants;

/**
 * @author Able
 * @date 2019/1/7
 */
public class SpHelper {
    private static Context mContext = CommonModule.getContext();

    public static void setUsername(String username) {
        SPUtil.putString(mContext, Constants.USERNAME, username);
    }

    public static String getUsername() {
        return SPUtil.getString(mContext, Constants.USERNAME, "");
    }
}

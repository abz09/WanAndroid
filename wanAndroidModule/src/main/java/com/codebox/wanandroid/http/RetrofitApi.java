package com.codebox.wanandroid.http;

import com.codebox.common.base.BaseResult;
import com.codebox.wanandroid.bean.ArticleBean;
import com.codebox.wanandroid.bean.BannerBean;
import com.codebox.wanandroid.bean.HotKeyBean;
import com.codebox.wanandroid.bean.TodoBean;
import com.codebox.wanandroid.bean.TreeBean;
import com.codebox.wanandroid.bean.UserBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * 网络接口定义
 *
 * @author Able
 * @date 2018/2/11
 */
public interface RetrofitApi {

    @GET("article/list/{pageNo}/json")
    Observable<BaseResult<ArticleBean>> queryArticle(@Path("pageNo") int pageNo);

    @GET("banner/json")
    Observable<BaseResult<List<BannerBean>>> getBanner();

    @GET("hotkey/json")
    Observable<BaseResult<List<HotKeyBean>>> getHotKey();

    @GET("tree/json")
    Observable<BaseResult<List<TreeBean>>> getTreeData();

    @GET("article/list/{pageNo}/json")
    Observable<BaseResult<ArticleBean>> getArticleTree(@Path("pageNo") int pageNo, @Query("cid") int cid);

    @FormUrlEncoded
    @POST("user/register")
    Observable<BaseResult<UserBean>> registerApp(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("user/login")
    Observable<BaseResult<UserBean>> loginApp(@FieldMap Map<String, Object> map);

    @GET("lg/todo/v2/list/{pageNo}/json")
    Observable<BaseResult<TodoBean>> getTodo(@Path("pageNo") int pageNo, @Query("status") int status);
}

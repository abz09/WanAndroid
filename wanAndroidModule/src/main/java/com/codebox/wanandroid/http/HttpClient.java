package com.codebox.wanandroid.http;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.codebox.common.base.BaseResult;
import com.codebox.common.base.Resource;
import com.codebox.common.http.RetrofitFactory;
import com.codebox.common.util.LogUtils;
import com.codebox.wanandroid.bean.ArticleBean;
import com.codebox.wanandroid.bean.BannerBean;
import com.codebox.wanandroid.bean.TodoBean;
import com.codebox.wanandroid.bean.TreeBean;
import com.codebox.wanandroid.bean.UserBean;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Able
 * @date 2018/3/20
 */
public class HttpClient {
    private static final HttpClient instance = new HttpClient();

    private HttpClient() {

    }

    public static HttpClient getInstance() {
        return instance;
    }

    private RetrofitApi api = RetrofitFactory.getInstance().create(RetrofitApi.class);

    /**
     * 查询文章列表
     *
     * @param pageNo 页码
     */
    public LiveData<Resource<ArticleBean>> queryArticle(int pageNo) {

        final MutableLiveData<Resource<ArticleBean>> articleLiveData = new MutableLiveData<>();
        articleLiveData.setValue(Resource.loading());

        api.queryArticle(pageNo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResult<ArticleBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BaseResult<ArticleBean> articleBeanBaseResult) {
                        articleLiveData.setValue(Resource.response(articleBeanBaseResult));
                    }

                    @Override
                    public void onError(Throwable e) {
                        articleLiveData.setValue(Resource.error(e));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return articleLiveData;
    }

    /**
     * 获取首页Banner
     */
    public LiveData<Resource<List<BannerBean>>> getBanner() {
        final MutableLiveData<Resource<List<BannerBean>>> bannerLiveData = new MutableLiveData<>();
        api.getBanner()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResult<List<BannerBean>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BaseResult<List<BannerBean>> listBaseResult) {
                        bannerLiveData.setValue(Resource.response(listBaseResult));
                    }

                    @Override
                    public void onError(Throwable e) {
                        bannerLiveData.setValue(Resource.error(e));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return bannerLiveData;
    }

    /**
     * 获取知识体系数据
     */
    public LiveData<Resource<List<TreeBean>>> getTreeData() {
        final MutableLiveData<Resource<List<TreeBean>>> liveData = new MutableLiveData<>();

        api.getTreeData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResult<List<TreeBean>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BaseResult<List<TreeBean>> listBaseResult) {
                        liveData.setValue(Resource.response(listBaseResult));
                    }

                    @Override
                    public void onError(Throwable e) {
                        liveData.setValue(Resource.error(e));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return liveData;
    }

    /**
     * 查询分类文章
     *
     * @param pageNo 页码
     * @param cid    分类ID
     */
    public LiveData<Resource<ArticleBean>> getArticleTree(int pageNo, int cid) {
        final MutableLiveData<Resource<ArticleBean>> articleLiveData = new MutableLiveData<>();
        articleLiveData.setValue(Resource.loading());

        api.getArticleTree(pageNo, cid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers
                        .mainThread())
                .subscribe(new Observer<BaseResult<ArticleBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BaseResult<ArticleBean> articleBeanBaseResult) {
                        articleLiveData.setValue(Resource.response(articleBeanBaseResult));
                    }

                    @Override
                    public void onError(Throwable e) {
                        articleLiveData.setValue(Resource.error(e));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return articleLiveData;
    }


    /**
     * 注册
     *
     * @param hashMap 传参map
     */
    public LiveData<Resource<UserBean>> registerApp(HashMap hashMap) {
        final MutableLiveData<Resource<UserBean>> liveData = new MutableLiveData<>();
        liveData.setValue(Resource.loading());

        api.registerApp(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResult<UserBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BaseResult<UserBean> userBeanBaseResult) {
                        liveData.setValue(Resource.response(userBeanBaseResult));
                    }

                    @Override
                    public void onError(Throwable e) {
                        liveData.setValue(Resource.error(e));
                    }

                    @Override
                    public void onComplete() {

                    }
                });

        return liveData;
    }

    /**
     * 用户登录
     *
     * @param map
     * @return
     */
    public LiveData<Resource<UserBean>> loginApp(HashMap map) {
        final MutableLiveData<Resource<UserBean>> liveData = new MutableLiveData<>();
        api.loginApp(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResult<UserBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BaseResult<UserBean> userBeanBaseResult) {
                        liveData.setValue(Resource.response(userBeanBaseResult));
                    }

                    @Override
                    public void onError(Throwable e) {
                        liveData.setValue(Resource.error(e));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return liveData;
    }

    public LiveData<Resource<UserBean>> registerAndLogin(HashMap hashMap) {
        final MutableLiveData<Resource<UserBean>> liveData = new MutableLiveData<>();
        api.registerApp(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(new Function() {
                    @Override
                    public Object apply(Object o) throws Exception {
                        return api.loginApp(hashMap);
                    }
                })
                .subscribe(new Observer<BaseResult<UserBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BaseResult<UserBean> userBeanBaseResult) {
                        liveData.setValue(Resource.response(userBeanBaseResult));
                    }

                    @Override
                    public void onError(Throwable e) {
                        liveData.setValue(Resource.error(e));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return liveData;
    }


    public LiveData<Resource<TodoBean>> getTodo(int pageNo, int status) {
        final MutableLiveData<Resource<TodoBean>> liveData = new MutableLiveData<>();

        api.getTodo(pageNo, status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResult<TodoBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BaseResult<TodoBean> todoBeanBaseResult) {
                        liveData.setValue(Resource.response(todoBeanBaseResult));
                        LogUtils.d(todoBeanBaseResult.toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        liveData.setValue(Resource.error(e));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return liveData;
    }

}

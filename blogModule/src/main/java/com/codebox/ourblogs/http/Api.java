package com.codebox.ourblogs.http;

import com.codebox.common.base.BaseResult;
import com.codebox.ourblogs.bean.BlogBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * @author Able
 * @date 2018/12/19
 */
public interface Api {

    @GET("page/{pageNo}")
    Call<BaseResult<List<BlogBean>>> getPageBlogs(@Path("pageNo") int pageNo);

    @POST("search")
    Observable<BaseResult<List<BlogBean>>> searchBlogs(@Body Map<String, Object> map);

}

package com.codebox.ourblogs.http;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Administrator
 * @date 2017/12/4
 */
public class RetrofitFactory {

    private static OkHttpClient mClient;
    private static Retrofit mRetrofit;

    private static final String HOST = "http://47.105.179.136:6060/api/blog/";
//    private static final String HOST = "http://192.168.9.110:6060/api/blog/";

    static {
        mClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .client(mClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

    }

    public static Retrofit getInstance() {
        return mRetrofit;
    }

}

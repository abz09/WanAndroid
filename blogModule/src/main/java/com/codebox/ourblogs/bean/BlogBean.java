package com.codebox.ourblogs.bean;

/**
 * @author Able
 * @date 2018/12/19
 */
public class BlogBean {

    /**
     * id : 84893305
     * title : 在组件module中databinding找不到怎么办？
     * type : 1
     * abbreviation : 在android studio中的自己创建的module中，找不到databinding找不到怎么办？1.看module的build.gradle文件中有没有声明databing:android {dataBinding {enabled true}...
     * link : https://blog.csdn.net/jiyidehao/article/details/84893305
     * author : jiyidehao
     * date : 2018-12-08 15:46:45
     * readCount : 40
     * commentCount : 0
     * content :
     */

    private int id;
    private String title;
    private int type;
    private String abbreviation;
    private String link;
    private String author;
    private String date;
    private int readCount;
    private int commentCount;
    private String content;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getReadCount() {
        return readCount;
    }

    public void setReadCount(int readCount) {
        this.readCount = readCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "BlogBean{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", abbreviation='" + abbreviation + '\'' +
                ", link='" + link + '\'' +
                ", author='" + author + '\'' +
                ", date='" + date + '\'' +
                ", readCount=" + readCount +
                ", commentCount=" + commentCount +
                ", content='" + content + '\'' +
                '}';
    }
}

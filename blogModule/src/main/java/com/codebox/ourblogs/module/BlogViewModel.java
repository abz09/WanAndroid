package com.codebox.ourblogs.module;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.codebox.common.base.Resource;
import com.codebox.common.util.LogUtils;
import com.codebox.ourblogs.bean.BlogBean;
import com.codebox.ourblogs.http.HttpClient;

import java.util.HashMap;
import java.util.List;

public class BlogViewModel extends ViewModel {
    private HttpClient mHttpClient = HttpClient.getInstance();
    private LiveData<Resource<List<BlogBean>>> pageLiveData;
    private LiveData<Resource<List<BlogBean>>> searchLiveData;

    /**
     * 查询一页博客
     *
     * @param pageNo
     * @return
     */
    public LiveData<Resource<List<BlogBean>>> getBlogs(int pageNo) {
        pageLiveData = mHttpClient.getPageBlog(pageNo);
        return pageLiveData;
    }

    public LiveData<Resource<List<BlogBean>>> searchBlogs(String keyword, int pageNo) {
        HashMap map = new HashMap<String, Object>();
        map.put("keyword", keyword);
        map.put("pageNo", pageNo);
        LogUtils.e(keyword + "  " + pageNo);
        searchLiveData = mHttpClient.searchBlog(map);
        return searchLiveData;
    }
}

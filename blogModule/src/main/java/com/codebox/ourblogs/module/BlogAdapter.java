package com.codebox.ourblogs.module;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.codebox.ourblogs.R;
import com.codebox.ourblogs.bean.BlogBean;

import java.util.List;

/**
 * @author Able
 * @date 2018/12/24
 */
public class BlogAdapter extends BaseQuickAdapter<BlogBean, BaseViewHolder> {
    public BlogAdapter(int layoutResId, @Nullable List<BlogBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, BlogBean item) {
        helper
                .setText(R.id.tv_title, item.getTitle())
                .setText(R.id.tv_abbreviation, item.getAbbreviation())
                .setText(R.id.tv_date, item.getDate())
                .setText(R.id.tv_read_count, "阅读数：" + item.getReadCount())
                .setText(R.id.tv_comment_count, "评论数：" + item.getCommentCount());
        if (item.getType() == 1) {
            helper.setText(R.id.tv_type, "原");
        } else {
            helper.setText(R.id.tv_type, "转");
        }
    }
}

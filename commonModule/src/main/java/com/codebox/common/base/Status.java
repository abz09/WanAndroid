package com.codebox.common.base;

/**
 * Created by 86839 on 2017/10/12.
 */

public enum Status {
    LOADING,//正在请求
    SUCCESS,//数据请求成功,状态码errorCode = 0
    FAILURE,//数据请求成功，状态码errorCode = 1
    ERROR,//网络错误或者请求数据失败（返回null）
}

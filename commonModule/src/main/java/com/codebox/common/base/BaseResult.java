package com.codebox.common.base;

/**
 * 封装接口返回数据结果集
 *
 * @author AbleZ
 */
public class BaseResult<T> {
    public static final int RESULT_SUCCESS = 0;

    private int errorCode;

    private String errorMsg;

    private T data;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return RESULT_SUCCESS == errorCode;
    }

    @Override
    public String toString() {
        return "BaseResult{" +
                "errorCode=" + errorCode +
                ", errorMsg='" + errorMsg + '\'' +
                ", data=" + data +
                '}';
    }
}

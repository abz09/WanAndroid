package com.codebox.common.util;
import android.util.Log;

import com.codebox.common.BuildConfig;


/**
 * @author Able
 * @date 2018/3/9
 * @description Log统一管理类
 */
public class LogUtils {

    private LogUtils() {
        /* cannot be instantiated */
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    private static final String TAG = " run ==>> ";

    /**
     * 下面是默认tag的函数
     */
    public static void i(String msg) {
        if (BuildConfig.DEBUG && msg != null) {
            Log.i(TAG, msg);
        }
    }

    public static void d(String msg) {
        if (BuildConfig.DEBUG && msg != null)
            Log.d(TAG, msg);
    }

    public static void e(String msg) {
        if (BuildConfig.DEBUG && msg != null)
            Log.e(TAG, msg);
    }

    public static void v(String msg) {
        if (BuildConfig.DEBUG && msg != null)
            Log.v(TAG, msg);
    }

    public static void w(String msg) {
        if (BuildConfig.DEBUG && msg != null)
            Log.w(TAG, msg);
    }

    /**
     * 下面是传入自定义tag的函数
     */
    public static void i(String tag, String msg) {
        if (BuildConfig.DEBUG && msg != null && tag != null) {
            Log.i(TAG + tag, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (BuildConfig.DEBUG && msg != null && tag != null)
            Log.d(TAG + tag, msg);
    }

    public static void e(String tag, String msg) {
        if (BuildConfig.DEBUG && msg != null && tag != null)
            Log.e(TAG + tag, msg);
    }

    public static void v(String tag, String msg) {
        if (BuildConfig.DEBUG && msg != null && tag != null)
            Log.v(TAG + tag, msg);
    }

    public static void w(String tag, String msg) {
        if (BuildConfig.DEBUG && msg != null && tag != null)
            Log.w(TAG + tag, msg);
    }

    /**
     * 以级别为 e 的形式输出LOG信息和Throwable
     */
    public static void e(Throwable t) {
        if (BuildConfig.DEBUG && t != null) {
            Log.e(TAG, "", t);
        }
    }

}

package com.codebox.common.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.codebox.common.common.CommonModule;


public class NetworkUtils {

    public static boolean isConnected() {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) CommonModule.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
        if (mNetworkInfo != null) {
            return mNetworkInfo.isAvailable();
        }
        return false;
    }

}

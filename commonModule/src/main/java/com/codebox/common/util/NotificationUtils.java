package com.codebox.common.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

/**
 * @author Able
 */
public class NotificationUtils extends ContextWrapper {
    public static final String channel_id = "channel_id";
    public static final String channel_name = "channel_name";

    private NotificationManager notificationManager;
    // 默认Notification 的Id
    private int mNotificationId = 0x0001;

    public int mIconId;
    public String mTitle;
    public String mContent;
    public int mProgress;
    public boolean mAutoCancel;
    public PendingIntent mPendingIntent;


    public NotificationUtils(Context context) {
        super(context);
    }

    public NotificationUtils(Context context, int id) {
        super(context);
        this.mNotificationId = id;
    }


    private NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }


    public NotificationUtils setTitle(String title) {
        this.mTitle = title;
        return this;
    }

    public NotificationUtils setContent(String content) {
        this.mContent = content;
        return this;
    }

    public NotificationUtils setProgress(int progress) {
        this.mProgress = progress;
        return this;
    }

    public NotificationUtils setAutoCancel(boolean autoCancel) {
        this.mAutoCancel = autoCancel;
        return this;
    }

    public NotificationUtils setIconId(int iconId) {
        this.mIconId = iconId;
        return this;
    }

    public void setPendingIntent(PendingIntent mPendingIntent) {
        this.mPendingIntent = mPendingIntent;
    }

    /**
     * 适配Android 8.0 及以上Notification
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getNotification_Oreo() {
        Notification.Builder builder = new Notification.Builder(getApplicationContext(), channel_id);
        builder
                .setContentTitle(mTitle)
                .setContentText(mContent)
                .setSmallIcon(mIconId)
                .setAutoCancel(mAutoCancel)
                .setContentIntent(mPendingIntent)
                .setDefaults(Notification.DEFAULT_ALL);
        if (mProgress > 0) {
            builder.setProgress(100, mProgress, false);
        }
        return builder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void createNotificationChannel() {
        NotificationChannel channel = new NotificationChannel(channel_id, channel_name, NotificationManager.IMPORTANCE_HIGH);
        getManager().createNotificationChannel(channel);
    }


    /**
     * 低版本的Notification
     */
    public NotificationCompat.Builder getNotification_Low() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channel_id);
        builder
                .setContentTitle(mTitle)
                .setContentText(mContent)
                .setSmallIcon(mIconId)
                .setAutoCancel(mAutoCancel)
                .setContentIntent(mPendingIntent)
                .setDefaults(Notification.DEFAULT_ALL);
        if (mProgress > 0) {
            builder.setProgress(100, mProgress, false);
        }
        return builder;
    }


    /**
     * 发送通知
     */
    public void sendNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
            Notification notification = getNotification_Oreo().build();
            getManager().notify(mNotificationId, notification);
        } else {
            Notification notification = getNotification_Low().build();
            getManager().notify(mNotificationId, notification);
        }
    }

    /**
     * 取消通知
     */
    public void cancel() {
        getManager().cancel(mNotificationId);
    }

}

package com.codebox.common.util;

import android.widget.Toast;

import com.codebox.common.common.CommonModule;

import es.dmoral.toasty.Toasty;

/**
 * 封装Toast工具类，使用第三方库Toasty
 *
 * @author Able
 * @date 2018/3/21
 */
public class ToastUtils {

    public static void showToast(CharSequence msg) {
        Toast.makeText(CommonModule.getApplication(), msg, Toast.LENGTH_SHORT).show();
    }

    public static void showInfo(CharSequence msg) {
        Toasty.info(CommonModule.getApplication(), msg, Toast.LENGTH_SHORT, false).show();
    }

    public static void showInfo(CharSequence msg, boolean withIcon) {
        Toasty.info(CommonModule.getApplication(), msg, Toast.LENGTH_SHORT, withIcon).show();
    }

    public static void showSuccess(CharSequence msg) {
        Toasty.success(CommonModule.getApplication(), msg, Toast.LENGTH_SHORT, false).show();
    }

    public static void showSuccess(CharSequence msg, boolean withIcon) {
        Toasty.success(CommonModule.getApplication(), msg, Toast.LENGTH_SHORT, withIcon).show();
    }

    public static void showWarning(CharSequence msg) {
        Toasty.warning(CommonModule.getApplication(), msg, Toast.LENGTH_SHORT, false).show();
    }

    public static void showWarning(CharSequence msg, boolean withIcon) {
        Toasty.warning(CommonModule.getApplication(), msg, Toast.LENGTH_SHORT, withIcon).show();
    }

    public static void showError(CharSequence msg) {
        Toasty.error(CommonModule.getApplication(), msg, Toast.LENGTH_SHORT, false).show();
    }

    public static void showError(CharSequence msg, boolean withIcon) {
        Toasty.error(CommonModule.getApplication(), msg, Toast.LENGTH_SHORT, withIcon).show();
    }

    public static void showError(CharSequence msg, int duration) {
        Toasty.warning(CommonModule.getApplication(), msg, duration).show();
    }
}

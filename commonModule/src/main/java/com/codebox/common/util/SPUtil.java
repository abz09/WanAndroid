package com.codebox.common.util;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * @author Able
 */
public class SPUtil {
    private static SharedPreferences sp;
    private static final String SP_DATA = "sp_data";

    private static SharedPreferences getSharedPreferences(Context context) {
        sp = context.getSharedPreferences(SP_DATA, Context.MODE_PRIVATE);
        return sp;
    }

    /**
     * 保存布尔数据
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putBoolean(Context context, String key, boolean value) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putBoolean(key, value).apply();
    }

    /**
     * 取布尔数据，返回的是false，默认值
     *
     * @param context
     * @param key
     * @return
     */
    public static boolean getBoolean(Context context, String key) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getBoolean(key, false);
    }

    /**
     * 取布尔数据,可以自定义值
     *
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static boolean getBoolean(Context context, String key, boolean defValue) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getBoolean(key, defValue);
    }

    /**
     * 删除布尔数据
     *
     * @param context
     * @param key
     */
    public static void removeBoolean(Context context, String key) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().remove(key).apply();
    }

    /**
     * 保存字符串
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putString(Context context, String key, String value) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(key, value).apply();
    }

    /**
     * 取字符串，返回的是传递过来的值
     *
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static String getString(Context context, String key, String defValue) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getString(key, defValue);
    }

    /**
     * 取字符串，返回的是Null
     *
     * @param context
     * @param key
     * @return
     */
    public static String getString(Context context, String key) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getString(key, null);
    }

    /**
     * 保存Long
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putLong(Context context, String key, long value) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putLong(key, value).apply();
    }

    /**
     * 取Long，返回的是-1
     *
     * @param context
     * @param key
     * @return
     */
    public static long getLong(Context context, String key) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getLong(key, -1);
    }

    /**
     * 保存整数
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putInt(Context context, String key, int value) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putInt(key, value).apply();
    }

    /**
     * 取整数，返回的是Null
     *
     * @param context
     * @param key
     * @return
     */
    public static int getInt(Context context, String key) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getInt(key, 0);
    }

    /**
     * 取整数，返回的是传递过来的值
     *
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static int getInt(Context context, String key, int defValue) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getInt(key, defValue);
    }

    /**
     * 移除某个key值已经对应的值
     *
     * @param context
     * @param key
     */
    public static void remove(Context context, String key) {
        SharedPreferences sp = getSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key).apply();
    }

    /**
     * 清除所有数据
     *
     * @param context
     */
    public static void clear(Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear().apply();
    }

    /**
     * 查询某个key是否已经存在
     *
     * @param context
     * @param key
     * @return
     */
    public static boolean contains(Context context, String key) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.contains(key);
    }
}

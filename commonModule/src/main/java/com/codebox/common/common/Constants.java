package com.codebox.common.common;

/**
 * @author Able
 * @date 2018/12/24
 */
public class Constants {
    public static final String EXTRA_WEB_TITLE = "EXTRA_WEB_TITLE";
    public static final String EXTRA_WEB_URL = "EXTRA_WEB_URL";
}

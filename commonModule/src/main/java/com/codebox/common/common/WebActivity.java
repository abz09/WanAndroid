package com.codebox.common.common;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.R;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.databinding.ActivityWebBinding;
import com.codebox.common.util.ToastUtils;
import com.just.agentweb.AgentWeb;

/**
 * @author Able
 * @date 2018/10/25
 */
@Route(path = RouterPath.CommonRouterPath.PATH_COMMON_WEBVIEW)
public class WebActivity extends BaseActivity<ActivityWebBinding> {

    private AgentWeb mAgentWeb;
    private String mUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        initView();
        initData();
    }


    protected void initView() {
        setNavigationIcon(R.drawable.ic_back);
    }

    protected void initData() {
        String mTitle = getIntent().getStringExtra(Constants.EXTRA_WEB_TITLE);
        mUrl = getIntent().getStringExtra(Constants.EXTRA_WEB_URL);

        setTitle(mTitle);
        loadUrl(mUrl);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_share, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void loadUrl(String url) {
        mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(mBinding.llContainer, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                .useDefaultIndicator(ContextCompat.getColor(this, R.color.colorPrimaryDark), 2)
                .setWebChromeClient(new WebChromeClient() {
                    @Override
                    public void onReceivedTitle(WebView view, String title) {
                        super.onReceivedTitle(view, title);
                        if (TextUtils.isEmpty(title)) {
                            setTitle(title);
                        }
                    }
                })
                .setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                    }
                })
                .createAgentWeb()
                .go(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_share) {
            ToastUtils.showInfo("share");
            clickShare();
        } else if (id == R.id.menu_open_browser) {
            ToastUtils.showInfo("browser");
            clickExplorer();
        }
        return super.onOptionsItemSelected(item);
    }

    private void clickShare() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, mUrl);
        startActivity(Intent.createChooser(intent, "分享"));
    }

    private void clickExplorer() {
        Uri uri = Uri.parse(mUrl);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}

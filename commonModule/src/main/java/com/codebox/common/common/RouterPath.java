package com.codebox.common.common;

/**
 * 路由路径
 * <p>
 * 不同组件中的路径在不同的静态内部类中，不同组件中的组名不能相同，（即第一级名字不能相同）
 *
 * @author 李立
 * @date 2018/2/7
 */
public class RouterPath {

    public static class BlogRouterPath {
        public static final String PATH_BLOG = "/blog/search";
    }

    public static class KnowledgeRouterPath {

        public static final String PATH_KNOWLEDGE_LIST = "/knowledge/list";

        public static final String PATH_CARDNO = "/knowledge/path/cardno";
        public static final String PATH_RXJAVA = "/knowledge/path/rxjava";
        public static final String PATH_SPEECH = "/knowledge/path/speech";
        public static final String PATH_CALENDAR = "/knowledge/path/calendar";
        public static final String PATH_FABDEMO = "/knowledge/path/fabDemo";
        public static final String PATH_TBS = "/knowledge/x5/tbs";
        public static final String PATH_RICH_TEXT = "/knowledge/path/richtext";
        public static final String PATH_COLLAPSING_TOOLBAR = "/knowledge/path/collapsingtoolbar";
        public static final String PATH_COLLAPSING_TOOLBAR2 = "/knowledge/path/collapsingtoolbar2";
        public static final String PATH_CUSTOM_BEHAVIOR = "/knowledge/path/custombehavior";
    }

    public static class WanAndroidRouterPath {

        public static final String PATH_MAIN_ACTIVITY = "/wan/main/mainactivity";

        public static final String PATH_TREE_ARTICLE = "/wan/module/tree/treeArticleActivity";

        public static final String PATH_LOGIN = "/wan/user/login_activity";
        /**
         * 关于我们
         */
        public static final String PATH_ABOUT_US = "/wan/about/aboutActivity";

        public static final String PATH_TEST2 = "/wan/test/test";

        public static final String PATH_HOTKEY = "/wan/hotkey/hotkey";

        public static final String PATH_TODOLIST = "/wan/todo/todolist";

    }

    public static class CommonRouterPath {
        public static final String PATH_COMMON_WEBVIEW = "/common/webview";
    }

}

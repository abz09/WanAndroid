package com.codebox.common.http;

import com.codebox.common.common.CommonModule;
import com.codebox.common.util.LogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Administrator
 * @date 2017/12/4
 */
public class RetrofitFactory {

    private static OkHttpClient client;
    private static Retrofit retrofit;

    //    private static final String HOST = "http://192.168.9.228:8087/express/api/";
    private static final String HOST = "https://www.wanandroid.com/";

    static {
        client = new OkHttpClient.Builder()
                .addInterceptor(new SaveCookiesInterceptor(CommonModule.getContext()))
                .addInterceptor(new AddCookiesInterceptor(CommonModule.getContext()))
                .connectTimeout(10, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public static Retrofit getInstance() {
        return retrofit;
    }
}

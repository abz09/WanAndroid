package com.codebox.common.imageloader;

import android.content.Context;
import android.widget.ImageView;

import com.codebox.common.util.glide.GlideUtil;
import com.youth.banner.loader.ImageLoader;

/**
 * Created by jingbin on 2016/11/30.
 * 首页轮播图
 */

public class GlideImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object url, ImageView imageView) {
        GlideUtil.load(context,url,imageView);
    }
}

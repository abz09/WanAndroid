package com.codebox.common.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.codebox.common.databinding.ViewLightEmptyBinding;
import com.codebox.common.databinding.ViewLightEmptyErrorBinding;


import com.codebox.common.R;
import com.codebox.common.util.NetworkUtils;

import pl.droidsonroids.gif.GifDrawable;

/**
 * 加载错误，网络占位图 ，暂无内容 View
 * @author 李立
 * @date 2018/12/8
 */
public class EmptyLayout extends FrameLayout {

    private OnResetListener mOnResetListener;
    private ViewLightEmptyBinding mBinding;

    public static final int NETWORK_ERROR = 1;
    public static final int NETWORK_LOADING = 2;
    public static final int NO_DATA = 3;
    public static final int HIDE_LAYOUT = 4;

    private GifDrawable mGifDrawable;

    private ViewLightEmptyErrorBinding errorBinding;

    public EmptyLayout(@NonNull Context context) {
        this(context, null);
    }

    public EmptyLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EmptyLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflater = LayoutInflater.from(context);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.view_light_empty, this, true);
        mGifDrawable = (GifDrawable) mBinding.gifLoading.getDrawable();

    }

    public void setNoDataContent(String tips) {
        mBinding.tvTips.setText(tips);
    }

    public View loadingView() {
        return mBinding.gifLoading;
    }

    public void setErrorType(int type) {

        switch (type) {
            case NO_DATA:
                setVisibility(VISIBLE);
                mBinding.gifLoading.setVisibility(View.GONE);
                mBinding.tvTips.setVisibility(View.VISIBLE);
                mGifDrawable.stop();
                break;
            case NETWORK_LOADING:
                if (errorBinding != null) {
                    errorBinding.getRoot().setVisibility(View.GONE);
                }
                setVisibility(VISIBLE);
                mBinding.tvTips.setVisibility(View.GONE);
                mBinding.gifLoading.setVisibility(View.VISIBLE);
                if (!mGifDrawable.isPlaying() && !mGifDrawable.isRecycled()) {
                    mGifDrawable.start();
                }
                break;
            case HIDE_LAYOUT:
                setVisibility(GONE);
                break;
            case NETWORK_ERROR:
                setVisibility(VISIBLE);
                mBinding.tvTips.setVisibility(View.GONE);
                mBinding.gifLoading.setVisibility(View.GONE);
                mGifDrawable.stop();

                ViewStub viewStub = mBinding.vsError.getViewStub();
                if (viewStub != null) {
                    viewStub.inflate();
                    errorBinding = (ViewLightEmptyErrorBinding) mBinding.vsError.getBinding();

                    if (NetworkUtils.isConnected()) {
                        errorBinding.tvTipsError.setText("数据加载失败,请稍后再试");
                    } else {
                        errorBinding.tvTipsError.setText("请检查您的手机是否联网");
                    }

                    if (mOnResetListener != null) {
                        errorBinding.btReset.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (NetworkUtils.isConnected()) {
                                    setErrorType(NETWORK_LOADING);
                                    mOnResetListener.onReset();
                                }
                            }
                        });
                    }
                }

                errorBinding.getRoot().setVisibility(View.VISIBLE);
                break;
        }
    }

    public TextView getTipsView() {
        return mBinding.tvTips;
    }

    public void setOnResetListener(OnResetListener listener) {
        mOnResetListener = listener;
    }

    public interface OnResetListener {
        void onReset();
    }

    @Override
    public void setBackgroundColor(int color) {
        super.setBackgroundColor(color);
        mBinding.getRoot().setBackgroundColor(color);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mGifDrawable != null && !mGifDrawable.isRecycled()) {
            mGifDrawable.recycle();
        }
    }
}

package com.able.wanandroid.app;

import android.app.Application;
import android.content.Context;

import com.able.wanandroid.BuildConfig;
import com.alibaba.android.arouter.launcher.ARouter;
import com.codebox.common.common.CommonModule;
import com.squareup.leakcanary.LeakCanary;


/**
 * @author Administrator
 */

public class BaseApplication extends Application {

    public Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);

        //初始化路由，这两句话必须卸载ARouter.init()之前
        if (BuildConfig.DEBUG) {
            ARouter.openLog();
            ARouter.openDebug();
        }
        ARouter.init(this);

        //初始化CommonModule
        CommonModule.init(this, BuildConfig.APPLICATION_ID);

        mContext = this;
    }
}

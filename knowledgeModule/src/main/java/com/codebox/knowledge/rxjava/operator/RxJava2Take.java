package com.codebox.knowledge.rxjava.operator;

import android.widget.TextView;

import com.codebox.common.util.LogUtils;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * take/takeLast 操作符，可以将上游事件中的前N项或者最后N项发送到下游,其他事件则进行过滤
 * 主要作用也是过滤
 *
 * @author 李立
 * @date 2019/1/1
 */
public class RxJava2Take {

    public TextView mTvLog;

    public RxJava2Take(TextView tvLog){
        mTvLog = tvLog;
    }

    public void testTake() {

        mTvLog.setText("");

        ArrayList<Integer> numList = new ArrayList<>();
        numList.add(1);
        numList.add(2);
        numList.add(3);
        numList.add(4);
        numList.add(5);

        Observable
                .fromIterable(numList)
                .take(3)
//                .takeLast(3)
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer integers) throws Exception {
//                        LogUtils.e("Rxjava2Take:" + integers.toString());
                        mTvLog.append("Rxjava2Take:" + integers.toString()+ "\n"+"\n");
                    }
                });

    }
}

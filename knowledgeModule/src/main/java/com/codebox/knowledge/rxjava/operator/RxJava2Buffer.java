package com.codebox.knowledge.rxjava.operator;

import android.widget.TextView;

import com.codebox.common.util.LogUtils;
import com.codebox.knowledge.rxjava.bean.Student;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;

/**
 * 它定期从Observable收集数据到一个集合，然后把这些数据集合打包发射，而不是一次发射一个
 * <p>
 * Rxjava2 Buffer 操作符和window 类似都是将每次发射一个数据改为每次发送多个数据
 *
 * @author 李立
 * @date 2019/1/1
 */
public class RxJava2Buffer {

    private TextView mTvLog;

    public RxJava2Buffer(TextView textView){
        this.mTvLog = textView;
    }

    /**
     * 每次发送4个数据，在onNext方法中每次收到的List<Student> 中包含4个元素。
     */
    public void testBuffer() {
        ArrayList<String> list = new ArrayList<>();
        list.add("小明1");
        list.add("小明2");
        list.add("小明3");
        list.add("小明4");
        list.add("小明5");
        list.add("小明6");
        list.add("小明7");
        list.add("小明8");
        list.add("小明9");
        list.add("小明10");

        mTvLog.setText("");

        Observable
                .fromIterable(list)
                .buffer(4)
                .map((Function<List<String>, List<Student>>) strings -> {

                    ArrayList<Student> students = new ArrayList<>();
                    int stringSize = strings.size();
                    for (int i = 0; i < stringSize; i++) {
                        Student student = new Student();
                        student.setName(strings.get(i));
                        students.add(student);
                    }
                    return students;
                })
                .subscribe(new Observer<List<Student>>() {
                               @Override
                               public void onSubscribe(Disposable d) {
//                                   LogUtils.e("Rxjava2Buffer:onSubScribe:");
                                   mTvLog.append("Rxjava2Buffer:onSubScribe:"+ "\n"+"\n");
                               }

                               @Override
                               public void onNext(List<Student> students) {
//                                   LogUtils.e("Rxjava2Buffer:onNext:");
//
//                                   LogUtils.e("Rxjava2Buffer:students:====" + students.toString());

                                   mTvLog.append("Rxjava2Buffer:onNext:"+ "\n"+"\n");
                                   mTvLog.append("Rxjava2Buffer:students:====" + students.toString()+ "\n"+"\n");
                               }

                               @Override
                               public void onError(Throwable e) {
//                                   LogUtils.e("Rxjava2Buffer:onError:");
                                   mTvLog.append("Rxjava2Buffer:onError:"+ "\n"+"\n");
                               }

                               @Override
                               public void onComplete() {
//                                   LogUtils.e("Rxjava2Buffer:onComplete:");
                                   mTvLog.append("Rxjava2Buffer:onComplete:"+ "\n"+"\n");
                               }
                           }
                );


    }
}

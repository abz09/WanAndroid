package com.codebox.knowledge.rxjava.operator;

import android.widget.TextView;

import com.codebox.common.util.LogUtils;
import com.codebox.knowledge.rxjava.bean.Student;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function3;

/**
 * zip功能
 *
 * @author 李立
 * @date 2019/1/1
 */
public class RxJava2Zip {

    private TextView mTvLog;
    public RxJava2Zip(TextView tvLog){
        mTvLog = tvLog;
    }

    /**
     * zip 单个对象，zipArray方法的调用理解有些问题，后续再写。
     * 单个对象的调用
     */
    public void testZip() {

        mTvLog.setText("");

        Observable observable11 = Observable.just("张");
        Observable observable22 = Observable.just("斌");
        Observable observable33 = Observable.just(30);
        Disposable subscribe = Observable.
                zip(observable11, observable22, observable33, (Function3<String, String, Integer, Student>) (s, s2, integer) -> {
                    Student student = new Student();
                    student.setName(s + s2);
                    student.setAge(integer);
                    return student;
                })
                .subscribe(new Consumer<Student>() {
                    @Override
                    public void accept(Student student) throws Exception {
//                        LogUtils.e("Rxjava2Zip:students:" + student.toString());
                        mTvLog.append("Rxjava2Zip:students:" + student.toString() + "\n"+"\n");
                    }
                });
    }

    /**
     * zip 操作集合 组合顺序和组合之前的数据中元素顺序是一样的
     */
    public void testZipList() {

        mTvLog.setText("");

        ArrayList<String> firstNameList = new ArrayList<>();
        ArrayList<String> lastNameList = new ArrayList<>();
        ArrayList<Integer> ageList = new ArrayList<>();


        firstNameList.add("李");
        firstNameList.add("张");
        firstNameList.add("王");
        firstNameList.add("赵");
        firstNameList.add("刘");

        lastNameList.add("力");
        lastNameList.add("芳");
        lastNameList.add("华");

        ageList.add(23);
        ageList.add(28);
        ageList.add(87);
        ageList.add(93);
        ageList.add(33);

        ArrayList<Student> studentList = new ArrayList<>();

        Observable<Integer> ageObservable = Observable.create(emitter -> {
            int ageSize = ageList.size();
            for (int i = 0; i < ageSize; i++) {
                emitter.onNext(ageList.get(i));
            }
        });

        Observable<String> firstNameObservable = Observable.create(emitter -> {
            int firstNameSize = firstNameList.size();
            for (int i = 0; i < firstNameSize; i++) {
                emitter.onNext(firstNameList.get(i));
            }
        });

        Observable<String> lastNameObservable = Observable.create(emitter -> {
            int lastNameSize = lastNameList.size();
            for (int i = 0; i < lastNameSize; i++) {
                emitter.onNext(lastNameList.get(i));
            }
        });

        Disposable subscribe = Observable
                .zip(firstNameObservable, lastNameObservable, ageObservable, (s, s2, integer) -> {
                    Student student = new Student(s + s2, integer);
                    return student;
                })
                .subscribe(student -> {
                    studentList.add(student);

                    if (studentList.size() >= 3) {
                        mTvLog.append("Rxjava2Zip:testZipList:" + studentList.toString() + "\n"+"\n");
//                        LogUtils.e("Rxjava2Zip:testZipList:" + studentList.toString());
                    }
                });
    }
}

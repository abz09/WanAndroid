package com.codebox.knowledge.rxjava.bean;

/**
 * 测试实体类
 *
 * @author 李立
 * @date 2018/12/31
 */
public class Teacher {
    private String name;
    private boolean isGood;
    private int goodNum;

    public Teacher() {
    }

    public Teacher(String name, boolean isGood) {
        this.name = name;
        this.isGood = isGood;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGood() {
        return isGood;
    }

    public void setGood(boolean good) {
        isGood = good;
    }

    public int getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(int goodNum) {
        this.goodNum = goodNum;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", isGood=" + isGood +
                ", goodNum=" + goodNum +
                '}';
    }
}

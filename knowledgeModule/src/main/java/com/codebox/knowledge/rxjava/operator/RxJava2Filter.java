package com.codebox.knowledge.rxjava.operator;

import android.widget.TextView;

import com.codebox.common.util.LogUtils;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;


/**
 * filter操作符 可以对集合中的数据过滤。
 *
 * @author 李立
 * @date 2018/12/31
 */
public class RxJava2Filter {

    private TextView mTvLog;
    public RxJava2Filter(TextView textView){
        mTvLog = textView;
    }

    public void testFilter() {

        mTvLog.setText("");

        ArrayList<Integer> numList = new ArrayList<>();
        numList.add(1);
        numList.add(2);
        numList.add(3);
        numList.add(4);
        numList.add(5);
        numList.add(6);

        ArrayList<Integer> evenList = new ArrayList<>();

        mTvLog.setText("");

        Disposable subscribe = Observable
                //fromIterable可以直接传集合，并且在subscribe 方法中回调的方法也不会多加一层List.
                .fromIterable(numList)

                //create方法可以对每个数据做更精细的操作
//                .create((ObservableOnSubscribe<Integer>) emitter -> {
//                            for (int i = 0; i < numList.size(); i++) {
//                                emitter.onNext(numList.get(i));
//                            }
//                        }
//                )
                .filter(integers -> {
                    if (integers % 2 == 0) {
                        return true;
                    } else {
                        return false;
                    }
                })
//                ,这里调用Observer只关心收到的onNext()不关系onComplete和onError
                .subscribe(integers -> {
                    evenList.add(integers);
//                    LogUtils.e("Rxjava2Filter:" + evenList.toString());
                    mTvLog.append("Rxjava2Filter:" + evenList.toString() + "\n"+"\n");
                });
    }
}

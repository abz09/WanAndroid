package com.codebox.knowledge.rxjava.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.common.RouterPath;
import com.codebox.knowledge.R;
import com.codebox.knowledge.databinding.ActivityRxjava2DemoBinding;
import com.codebox.knowledge.rxjava.operator.RxJava2Buffer;
import com.codebox.knowledge.rxjava.operator.RxJava2Distinct;
import com.codebox.knowledge.rxjava.operator.RxJava2Filter;
import com.codebox.knowledge.rxjava.operator.RxJava2OfType;
import com.codebox.knowledge.rxjava.operator.RxJava2Reduce;
import com.codebox.knowledge.rxjava.operator.RxJava2Take;
import com.codebox.knowledge.rxjava.operator.RxJava2Window;
import com.codebox.knowledge.rxjava.operator.RxJava2Zip;
import com.codebox.knowledge.rxjava.operator.Rxjava2Map;
import com.codebox.knowledge.rxjava.operator.Rxjava2Scan;

/**
 * @author Able
 * @date 2019/1/2
 */

@Route(path = RouterPath.KnowledgeRouterPath.PATH_RXJAVA)
public class RxJava2DemoActivity extends BaseActivity<ActivityRxjava2DemoBinding> implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rxjava2_demo);

        initView();
        initListener();
    }

    @Override
    public boolean isChangeOrientationToPortrait() {
        return true;
    }

    private void initView() {
        setTitle("RxJava2Demo");
        setNavigationIcon(R.drawable.ic_back);
    }

    private void initListener() {
        mBinding.btnMap.setOnClickListener(this);
        mBinding.btnBuffer.setOnClickListener(this);
        mBinding.btnDistinct.setOnClickListener(this);
        mBinding.btnFilter.setOnClickListener(this);
        mBinding.btnReduce.setOnClickListener(this);
        mBinding.btnScan.setOnClickListener(this);
        mBinding.btnTake.setOnClickListener(this);
        mBinding.btnWindow.setOnClickListener(this);
        mBinding.btnZip.setOnClickListener(this);
        mBinding.btnOfType.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_map) {
            testMap();
        } else if (id == R.id.btn_filter) {
            testFilter();
        } else if (id == R.id.btn_zip) {
            testZip();
        } else if (id == R.id.btn_distinct) {
            testDistinct();
        } else if (id == R.id.btn_scan) {
            testScan();
        } else if (id == R.id.btn_buffer) {
            testBuffer();
        } else if (id == R.id.btn_window) {
            testWindow();
        } else if (id == R.id.btn_reduce) {
            testReduce();
        } else if (id == R.id.btn_take) {
            testTake();
        } else if(id == R.id.btn_of_type){
            testOfType();
        }else{

        }
    }

    private void testOfType(){
        RxJava2OfType type = new RxJava2OfType(mBinding.tvLog);
        type.testOfType();
    }

    private void testMap() {
        Rxjava2Map rxjava2Map = new Rxjava2Map(mBinding.tvLog);
        rxjava2Map.flatMapMethod();
        rxjava2Map.concatMap();
    }

    private void testFilter() {
        RxJava2Filter filter = new RxJava2Filter(mBinding.tvLog);
        filter.testFilter();
    }

    private void testBuffer() {
        RxJava2Buffer buffer = new RxJava2Buffer(mBinding.tvLog);
        buffer.testBuffer();
    }

    private void testWindow() {
        RxJava2Window window = new RxJava2Window(mBinding.tvLog);
        window.testWindow();
    }

    private void testDistinct() {
        RxJava2Distinct distinct = new RxJava2Distinct(mBinding.tvLog);
        distinct.testDistinct();
    }

    private void testTake() {
        RxJava2Take take = new RxJava2Take(mBinding.tvLog);
        take.testTake();
    }

    private void testReduce() {
        RxJava2Reduce reduce = new RxJava2Reduce(mBinding.tvLog);
        reduce.testReduce();
    }

    private void testScan() {
        Rxjava2Scan scan = new Rxjava2Scan(mBinding.tvLog);
        scan.testScanRule(Rxjava2Scan.Rule.Add,mBinding.tvLog);
        scan.testScanRule(Rxjava2Scan.Rule.SUB,mBinding.tvLog);
        scan.testScanRule(Rxjava2Scan.Rule.MULTI,mBinding.tvLog);
        scan.testScanRule(Rxjava2Scan.Rule.DIV,mBinding.tvLog);
    }

    private void testZip() {
        RxJava2Zip zip = new RxJava2Zip(mBinding.tvLog);
        zip.testZip();
        zip.testZipList();
    }

}

package com.codebox.knowledge.rxjava.operator;

import android.widget.TextView;

import com.codebox.common.util.LogUtils;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * reduce操作符 和scan 操作符都可做累加功能
 * <p>
 * 字面意思减少，也可以叫“聚合”或者“压缩”
 * 实际上的效果就是可以把一个被观察者中的多个事件进行压缩，最后发射压缩后的事件
 * 其实就是只发送最后一个数据
 * 而scan 则是每次都发送
 *
 * @author 李立
 * @date 2019/1/1
 */
public class RxJava2Reduce {

    public TextView mTvLog;

    public RxJava2Reduce(TextView textView) {
        mTvLog = textView;
    }

    public void testReduce() {

        mTvLog.setText("");

        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);


        Disposable subscribe = Observable
                .fromIterable(list)
                //有了reduce,accept方法只走最后一次，没有reduce，accept方法会被调用10次。
                .reduce((t1, t2) -> t1 + t2)
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer integer) throws Exception {
//                        LogUtils.e("Rxjava2Reduce:" + integer);
                        mTvLog.append("Rxjava2Reduce:" + integer + "\n"+"\n");
                    }
                });
    }


}

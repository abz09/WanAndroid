package com.codebox.knowledge.rxjava.operator;

import android.util.Log;
import android.widget.TextView;

import com.codebox.knowledge.rxjava.bean.Student;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * @author 李立
 * @date 2019/1/6
 */
public class RxJava2OfType {

    public TextView mTvLog;

    public RxJava2OfType(TextView textView){
        this.mTvLog = textView;
    }

    public void testOfType(){
        mTvLog.setText("");
        ArrayList<Object> list = new ArrayList<>();
        list.add(10);
        list.add("小丹");
        list.add(0.12345);
        list.add(new Student("小霞",12));

        Observable.fromIterable(list)
                .ofType(Student.class)
                .subscribe(new Consumer<Student>() {
                    @Override
                    public void accept(Student item) throws Exception {

                        mTvLog.append("Rxjava2OfType：subscribe:"+ item.toString() +"\n" + "\n");
                    }
                });
    }
}

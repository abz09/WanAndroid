package com.codebox.knowledge.rxjava.operator;

import android.widget.TextView;

import com.codebox.common.util.LogUtils;
import com.codebox.knowledge.rxjava.bean.Student;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * distinct 操作符，去除重复对象。
 *
 * @author 李立
 * @date 2019/1/1
 */
public class RxJava2Distinct {

    public TextView mTvLog;

    public RxJava2Distinct(TextView textView){
        this.mTvLog = textView;
    }

    /**
     * 这个没有去除掉重复元素，可能自定义类似Comparator类似的东西。
     */
    public void testDistinct() {

        mTvLog.setText("");

        ArrayList<Student> studentList = new ArrayList<>();

        Student student1 = new Student("张一", 91);
        Student student2 = new Student("张二", 92);
        Student student3 = new Student("张三", 93);

        studentList.add(student1);
        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        studentList.add(student3);
        studentList.add(student1);

        Observable
                .fromIterable(studentList)
                .distinct()
                .subscribe(new Consumer<Student>() {
                               @Override
                               public void accept(Student student) throws Exception {
                                   // 只会输出没有输出过的数据
//                                   LogUtils.e("Rxjava2Distinct:" + student.toString()
//                                   );
//
                                   mTvLog.append("Rxjava2Distinct:" + student.toString() + "\n"+"\n");
                               }
                           }
                );
    }
}

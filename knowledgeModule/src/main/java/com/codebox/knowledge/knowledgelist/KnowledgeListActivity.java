package com.codebox.knowledge.knowledgelist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.common.RouterPath;
import com.codebox.common.util.GsonUtils;
import com.codebox.common.util.ToastUtils;
import com.codebox.knowledge.R;
import com.codebox.knowledge.databinding.ActivityKnowledgeListBinding;

import java.util.List;
import java.util.Map;


/**
 * @author Able
 */
@Route(path = RouterPath.KnowledgeRouterPath.PATH_KNOWLEDGE_LIST)
public class KnowledgeListActivity extends BaseActivity<ActivityKnowledgeListBinding> {

    private List<Map<String, Object>> mKnowledgeList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knowledge_list);

        initView();
        initData();
        initAdapter();
    }

    private void initView() {
        setTitle("知识列表");
        setNavigationIcon(R.drawable.ic_back);
    }

    private void initData() {
        String knowledgeStr = GsonUtils.getJsonFromAssets(this, "knowledge.json");
        mKnowledgeList = GsonUtils.getListMap(knowledgeStr);
    }

    private void initAdapter() {
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        mBinding.recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        KnowledgeAdapter mKnowledgeAdapter = new KnowledgeAdapter(R.layout.item_knowledge_list, mKnowledgeList);
        mBinding.recyclerView.setAdapter(mKnowledgeAdapter);

        mKnowledgeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                String routerPath = (String) mKnowledgeList.get(position).get("routerPath");

                dealClick(routerPath);
            }
        });
    }

    private void dealClick(String routerPath) {
        if (!TextUtils.isEmpty(routerPath)) {
            try {
                ARouter.getInstance().build(routerPath).navigation();
            } catch (Exception e) {
                ToastUtils.showError("跳转出错~~");
            }
        } else {
            ToastUtils.showWarning("不支持跳转~~");
        }
    }

}
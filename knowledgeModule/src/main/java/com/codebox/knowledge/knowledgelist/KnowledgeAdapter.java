package com.codebox.knowledge.knowledgelist;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.codebox.knowledge.R;

import java.util.List;
import java.util.Map;

public class KnowledgeAdapter extends BaseQuickAdapter<Map<String, Object>, BaseViewHolder> {
    public KnowledgeAdapter(int layoutResId, @Nullable List<Map<String, Object>> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String, Object> item) {
        helper
                .setText(R.id.tv_title, item.get("title").toString());
    }

}

package com.codebox.knowledge.calendar;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.DatePicker;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.common.RouterPath;
import com.codebox.knowledge.R;
import com.codebox.knowledge.databinding.ActivityCalendarBinding;

import java.util.Calendar;

/**
 * @author Able
 * @date 2019/1/3
 */

@Route(path = RouterPath.KnowledgeRouterPath.PATH_CALENDAR)
public class CalendarActivity extends BaseActivity<ActivityCalendarBinding> {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        initView();
    }

    private void initView() {

        mBinding.etDate.setOnClickListener(v -> showCalendar());
    }

    private void showCalendar() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        mBinding.etDate.setText(new StringBuilder()
                                .append(year)
                                .append("-")
                                .append((month + 1) < 10 ? "0" + (month + 1) : (month + 1))
                                .append("-")
                                .append((day < 10) ? "0" + day : day));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

//        DatePicker datePicker = datePickerDialog.getDatePicker();
//        Date date = new Date();//当前时间
//        long time = date.getTime();
//        datePicker.setMaxDate(time);

//        datePickerDialog.setContentView();
        datePickerDialog.show();
    }
}

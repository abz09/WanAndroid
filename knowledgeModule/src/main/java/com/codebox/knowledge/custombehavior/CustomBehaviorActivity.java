package com.codebox.knowledge.custombehavior;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.common.RouterPath;
import com.codebox.knowledge.R;

/**
 * @author Able
 * @date 2019/5/23
 */
@Route(path = RouterPath.KnowledgeRouterPath.PATH_CUSTOM_BEHAVIOR)
public class CustomBehaviorActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_custom_behavior);
    }
}

package com.codebox.knowledge.collapsingtoolbar;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.common.RouterPath;
import com.codebox.knowledge.R;
import com.codebox.knowledge.databinding.ActivityCollapsingToolbar2Binding;

/**
 * @author Able
 * @date 2019/5/17
 */

@Route(path = RouterPath.KnowledgeRouterPath.PATH_COLLAPSING_TOOLBAR2)
public class CollapsingToolbarActivity2 extends AppCompatActivity {

    ActivityCollapsingToolbar2Binding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_collapsing_toolbar2);

        initView();

    }

    private void initView() {
    }
}

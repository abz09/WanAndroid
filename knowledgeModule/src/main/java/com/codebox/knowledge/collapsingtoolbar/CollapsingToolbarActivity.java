package com.codebox.knowledge.collapsingtoolbar;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.base.EventMsg;
import com.codebox.common.common.RouterPath;
import com.codebox.common.util.LogUtils;
import com.codebox.common.util.RxBus;
import com.codebox.knowledge.R;
import com.codebox.knowledge.databinding.ActivityCollapsingToolbarBinding;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * @author Able
 * @date 2019/5/17
 */

@Route(path = RouterPath.KnowledgeRouterPath.PATH_COLLAPSING_TOOLBAR)
public class CollapsingToolbarActivity extends AppCompatActivity {

    ActivityCollapsingToolbarBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_collapsing_toolbar);

        initView();

    }

    private void initView() {
        mBinding.tvName.setText("123");

        Disposable disposable = RxBus.getInstance().doSubscribe(Float.class, new Consumer<Float>() {
            @Override
            public void accept(Float aFloat) throws Exception {
                LogUtils.e(aFloat + "");
                if (aFloat == 0) {
                    mBinding.toolbar.setTitle("CustomBehavior");
                } else {
                    mBinding.toolbar.setTitle("");
                }
            }
        });
        RxBus.getInstance().addSubscription(this, disposable);
    }
}

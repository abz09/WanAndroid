package com.codebox.knowledge.floatingactionbutton;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.codebox.common.base.BaseActivity;
import com.codebox.common.common.RouterPath;
import com.codebox.common.util.ToastUtils;
import com.codebox.knowledge.R;
import com.codebox.knowledge.databinding.ActivityFabDemoBinding;

/**
 * @author Able
 * @date 2019/1/10
 */
@Route(path = RouterPath.KnowledgeRouterPath.PATH_FABDEMO)
public class FabDemoActivity extends BaseActivity<ActivityFabDemoBinding> implements View.OnClickListener {


    private boolean isOpen = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fab_demo);

        initView();
        initListener();
    }


    private void initView() {
        setTitle("FloatingActionButton");
        setNavigationIcon(R.drawable.ic_back);
    }

    private void initListener() {
        mBinding.fabAdd.setOnClickListener(this);
        mBinding.ll01.setOnClickListener(this);
        mBinding.ll02.setOnClickListener(this);
        mBinding.ll03.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.fab_add) {
            dealFab();

        } else if (id == R.id.ll01) {
            ToastUtils.showSuccess("01");
        } else if (id == R.id.ll02) {
            ToastUtils.showSuccess("02");
        } else if (id == R.id.ll03) {
            ToastUtils.showSuccess("03");
        }
    }

    private void dealFab() {
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator rotation;
        ObjectAnimator translate;
        ObjectAnimator alpha;
        ObjectAnimator scale;

        if (!isOpen) {
            mBinding.llOperator.setVisibility(View.VISIBLE);

            rotation = ObjectAnimator.ofFloat(mBinding.fabAdd, "rotation", 0f, 45f);
            translate = ObjectAnimator.ofFloat(mBinding.llOperator, "translationY", 800f, 0f);
            alpha = ObjectAnimator.ofFloat(mBinding.llOperator, "alpha", 0f, 1f);
            scale = ObjectAnimator.ofFloat(mBinding.llOperator, "scaleY", 0f, 1f);

        } else {

            rotation = ObjectAnimator.ofFloat(mBinding.fabAdd, "rotation", 45f, 90f);
            translate = ObjectAnimator.ofFloat(mBinding.llOperator, "translationY", 0f, 800f);
            alpha = ObjectAnimator.ofFloat(mBinding.llOperator, "alpha", 1f, 0f);
            scale = ObjectAnimator.ofFloat(mBinding.llOperator, "scaleY", 1f, 0f);
        }

        animatorSet.playTogether(rotation, alpha, translate);
        animatorSet.setDuration(200);
        animatorSet.setInterpolator(new OvershootInterpolator());
        animatorSet.start();

        if (isOpen) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBinding.llOperator.setVisibility(View.GONE);
                }
            }, 200);
        }

        // 最后切换状态
        isOpen = !isOpen;
    }
}
